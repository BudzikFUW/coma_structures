Porting to python 3.10, removing unneeded stuff.

Checked and works under Python 3.10
new pipeline:
- if needed: convert_edf_to_raw
- decompose raw (can use multi_ssh_processing) examples
- fit_dipole_to_atoms (can use multi_ssh_processing) - to fit dipole to non transverse montage atoms
- mark_structures_sanity_check_lite.py
- research/tagged_spindle_dipole_analysis.py




All rest of the rest, might not work:
for marking:
- if needed: convert_edf_to_raw
- decompose raw (can use multi_ssh_processing) examples
- fit_dipole_to_atoms (can use multi_ssh_processing) - to fit dipole to non transverse montage atoms
- mark_important_atoms - mark good atoms in examples created
- research_important_atoms - visualise selected good atoms, create training CSV, and full parametrised atom databases for 2nd layer neural net training
- train_nn - train neural net based on the selected good atoms, and second layer neural net
- mark_structures_nn - use trained neural nets to mark tags on a signal
- tag_research/compare_tag_folders - to test and compare tags

new pipeline:

- if needed: convert_edf_to_raw
- decompose raw (can use multi_ssh_processing) examples
- fit_dipole_to_atoms (can use multi_ssh_processing) - to fit dipole to non transverse montage atoms
- mark_structures_sanity_check_lite.py
- research/tagged_spindle_dipole_analysis.py

