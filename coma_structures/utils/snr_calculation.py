import numpy as np
from tqdm import tqdm


def calculate_atom_neighborhood_snr(atoms):
    """calculates SNR for every atom, looking at it's neighborhood per channel.
    ATOMS MUST BE SORTED BY ABSOLUTE TIME!!!!
    """

    """take into account atoms which have neighbors inside atom width x2
    TODO: additionally grab other atoms which are big, so big, that they are outside of this atom?
    """
    channel_counts = atoms.ch_id.max()
    all_snrs = np.zeros(atoms.shape[0])
    for channel in tqdm(range(1, channel_counts + 1), desc='calculating SNR per channel', total=channel_counts):
        fitting_channel_mask = atoms.ch_id == channel
        fitting_atoms = atoms.loc[fitting_channel_mask]
        atoms_starts = (fitting_atoms.absolute_position - fitting_atoms.width).values
        atoms_stops = (fitting_atoms.absolute_position + fitting_atoms.width).values
        atoms_centers = fitting_atoms.absolute_position.values
        atoms_energy = fitting_atoms.modulus.values ** 2

        snrs = np.zeros(len(atoms_starts))
        for i in range(len(atoms_starts)):
            ind_start, ind_stop = np.searchsorted(atoms_centers, [atoms_starts[i], atoms_stops[i]])
            neighbourhood_energy = np.sum(atoms_energy[ind_start:ind_stop])
            own_energy = atoms_energy[i]
            snrs[i] = 10 * np.log(own_energy/neighbourhood_energy)
        all_snrs[fitting_channel_mask] = snrs
    atoms_with_snr = atoms.assign(snr=all_snrs)
    return atoms_with_snr