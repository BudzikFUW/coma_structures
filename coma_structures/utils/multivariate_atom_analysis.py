import cmath
import glob
import os

import numpy as np
import pandas
from astropy.stats import circmean
from mne import fit_dipole, create_info, EvokedArray, make_ad_hoc_cov


from coma_structures.old.config import RESAMPLING_FREQUENCY
from coma_structures.utils.interpolate_head_features import INTERP_SIZE
from coma_structures.utils.utils import gabor
from scipy.ndimage import center_of_mass
from scipy.signal import convolve2d
from scipy.stats import circstd

from coma_structures.mark_important_atoms import interpolate_feature_for_visualisation
from PIL import Image

# expected dipole locations in head space
k_comp_c = np.array([0.0024252103820732902, -0.002931163918627026, 0.07660861911409328])
spindle_c = np.array([-0.003142974982585871, 0.03321813933824179, 0.07466962480072538])
vsw_c = np.array([0.005206022245592502, 0.01795458389567986, 0.08774658147289283])


def distance(pos1, pos2):
    """3D points on someone head
    pos2 center, sign is y axis - positive when pos1 is in front of pos2"""
    # if pos2[1] < pos1[1]:
    #     sign = 1
    # else:
    #     sign = -1
    return np.sqrt(np.sum((pos1 - pos2) ** 2)) #  * sign


def reconstruct_multichannel_atom(example_atoms, timeline):
    reconsctruction = np.zeros((timeline.shape[0], example_atoms.amplitude.shape[0]))

    for nr, atom in enumerate(example_atoms.itertuples()):
        # atom amplitudes are loaded as p2p
        atom_s = atom.amplitude / 2.0 * gabor(timeline,
                                        atom.width,
                                        atom.absolute_position,
                                        atom.frequency,
                                        atom.phase,
                                        )
        reconsctruction[:, nr] = atom_s
    return reconsctruction


def amplitude_signs(example, debug=False):
    weights = example.amplitude.values / np.sum(example.amplitude.values) * len(example.amplitude.values)
    dominant_polar_plot_direction = circmean(example.phase.values, weights=weights)
    seperating_plot_direction = (dominant_polar_plot_direction + np.pi / 2) % (np.pi * 2)
    rotated_to_seperating_line = (example.phase - seperating_plot_direction) % (np.pi * 2)
    signs = []
    for phase in rotated_to_seperating_line:
        if 0 < phase <= np.pi:
            signs.append(1)
        else:
            signs.append(-1)
    if debug:
        timeline = np.arange(example.absolute_position.values[0]-2, example.absolute_position.values[0]+2, 0.001)
        reconstruction = reconstruct_multichannel_atom(example, timeline)
        colors = []
        for i in signs:
            if i >= 0:
                colors.append('red')
            else:
                colors.append('blue')
        import pylab as pb
        for i in range(len(example)):
            pb.plot(timeline, reconstruction[:, i], color=colors[i])
        fig, ax = pb.subplots(subplot_kw={'projection': 'polar'})
        ax.scatter(example.phase.values, example.amplitude.values, color=colors)
        ax.plot([dominant_polar_plot_direction, dominant_polar_plot_direction + np.pi], [np.max(example.amplitude), np.max(example.amplitude)], color='black')
        ax.plot([seperating_plot_direction, seperating_plot_direction + np.pi],
                [np.max(example.amplitude), np.max(example.amplitude)], color='red')
        phase_variability_value = phase_variability(example, amp_threshold_setting=0)
        phase_variability_value_reversals = phase_variability_reversals(example, amp_threshold_setting=0)
        ax.set_title('Phase var: {}\n Phase var: rev {}'.format(phase_variability_value * 180/np.pi, phase_variability_value_reversals* 180/np.pi))
        pb.show()
    return np.array(signs)


def example_to_dipole(example, channels, ref_channel='Oz', fs_dir=''):
    if ref_channel == 'average':
        ref_channels = []
    else:
        ref_channels = ref_channel.strip().split(',')
        if len(ref_channels) == 1 and ref_channels[0] == '':
            ref_channels = []
    missing_channels = list(set(ref_channels) - set(channels))
    channels_with_ref = channels + missing_channels
    bem_file = os.path.join(fs_dir, 'bem', 'fsaverage-5120-5120-5120-bem-sol.fif')
    trans_file = os.path.join(fs_dir, 'bem', 'fsaverage-trans.fif')
    info = create_info(channels_with_ref, RESAMPLING_FREQUENCY, ch_types='eeg', verbose='CRITICAL')
    signs = amplitude_signs(example)
    missing_channels_count = len(missing_channels)
    data = np.array(list(example.amplitude.values * signs) + [0.0, ] * missing_channels_count)[:, None] * 1e-6
    evoked = EvokedArray(data,
                         info,
                         verbose='CRITICAL')

    if len(ref_channels) == 0:
        evoked.set_eeg_reference('average', verbose='CRITICAL')
    else:
        evoked.set_eeg_reference(ref_channels, verbose='CRITICAL')
    evoked.set_montage('standard_1005', verbose='CRITICAL')
    covariance = make_ad_hoc_cov(info)
    dip, residual = fit_dipole(evoked, covariance, bem_file, trans=trans_file, verbose='CRITICAL', min_dist=0,
                               tol=5e-03)
    return dip
    # dip.plot_locations(trans=trans_file, subjects_dir=os.path.join(fs_dir, '..'), subject='fsaverage')


def parametrise_atom(example):
    """Take MMP atom (all channels) and create parametrisation
    example []

    created params:
    Atom params from MP
    Center of mass
    convolution filters
    phase variability

    Labels are created using parametrisation_labels function

    """
    # try sorting by SNR
    amp_mean = example.amplitude.mean()
    amp_std = example.amplitude.std()
    try:
        amp_z_mean = example.amplitude_z_score.mean()
        amp_z_std = example.amplitude_z_score.std()
    except AttributeError:
        amp_z_mean = np.nan
        amp_z_std = np.nan
    # strongest_atom_channel_id = example.snr.values.argmax()
    strongest_atom_channel_id = example.modulus.values.argmax()
    example_for_visualisation = example.iloc[strongest_atom_channel_id]

    # In [14]: to_median.groupby("label_manual_marking").median()
    # Out[14]:
    #                           frequency
    # label_manual_marking
    # Kompleks K                   1.3750
    # None                         7.6250
    # Spindle (wrzeciono)         12.5625
    # Wierzchołkowa fala ostra     2.5625

    distance_from_12_5 = np.abs(example_for_visualisation['frequency'] - 12.5)
    distance_from_1_37 = np.abs(example_for_visualisation['frequency'] - 1.37)
    distance_from_2_56 = np.abs(example_for_visualisation['frequency'] - 2.56)

    if example_for_visualisation['frequency'] == 0:  # special case - only gaussian envelope
        cycles_count = 0.5  # half a cycle, one peak, without valley
    else:
        period = 1 / example_for_visualisation['frequency']
        cycles_count = example_for_visualisation['width'] / period

    phase_variability_value = phase_variability(example)

    phase_variability_value_reversals = phase_variability_reversals(example)

    dip_pos = example[['dip_posx', 'dip_posy', 'dip_posz']].values[0]

    dip_distance_to_k_comp = distance(dip_pos, k_comp_c)
    dip_distance_to_spindle = distance(dip_pos, spindle_c)
    dip_distance_to_vsw = distance(dip_pos, vsw_c)

    series = pandas.Series(
        [amp_mean, amp_std, amp_z_mean, amp_z_std,
         distance_from_12_5, distance_from_1_37, distance_from_2_56,
         cycles_count,
         phase_variability_value,
         phase_variability_value_reversals, ] +
        [dip_distance_to_k_comp,
         dip_distance_to_spindle,
         dip_distance_to_vsw],
        )

    result = pandas.concat([example_for_visualisation, series])
    return result


def parametrisation_labels():
    """Helper functions for parametrise_atom"""
    example_names = []

    example_names.append('amp_mean')
    example_names.append('amp_std')
    example_names.append('amp_z_mean')
    example_names.append('amp_z_std')
    example_names.append('frequency_distance_from_12_5')
    example_names.append('frequency_distance_from_1_37')
    example_names.append('frequency_distance_from_2_56')
    example_names.append('cycles_count')
    example_names.append('phase_variability')
    example_names.append('phase_variability_reversals')

    example_names.append('dip_dist_kcomp')
    example_names.append('dip_dist_spindle')
    example_names.append('dip_dist_vsw')

    return example_names


def phase_variability(example, amp_threshold_setting=0.5):
    max_amp = np.max(example.amplitude.values)
    amp_threshold = max_amp * amp_threshold_setting
    amp_mask = (example.amplitude.values > amp_threshold)
    phases = example.phase.values[amp_mask]
    phase_variability_value = circstd(phases, high=np.pi, low=-np.pi)
    return phase_variability_value


def phase_variability_reversals(example, amp_threshold_setting=0.5):
    max_amp = np.max(example.amplitude.values)
    amp_threshold = max_amp * amp_threshold_setting
    amp_mask = (example.amplitude.values > amp_threshold)
    phases = example.phase.values[amp_mask]
    phase_variability_value = circstd(phases, high=np.pi, low=0)
    return phase_variability_value


def get_kernels(montage):
    # import IPython
    # IPython.embed()
    assert montage in ['none', 'transverse']
    kernels = []
    for kernel in glob.glob(os.path.join(os.path.dirname(__file__), 'kernels', montage, '*.png')):
        img = Image.open(kernel)
        img = np.array(img)[:, :, 0].astype(np.float64)
        img = np.flip(img, axis=0)
        img = img / np.sum(img)
        name = os.path.splitext(os.path.basename(kernel))[0]
        kernels.append([img, name])

    return kernels


def convolution_params(example, channels, kernels, datas=None):
    if datas is None:
        datas = interpolate_feature_for_visualisation(example,
                                                      channels,
                                                      )
        datas[np.logical_not(np.isfinite(datas))] = 0
    datas = datas / np.max(datas)
    convolution_params = []
    for kernel, kernel_name in kernels:
        activations = convolve2d(datas, kernel, mode='valid')
        convolution_params.append(np.max(activations))
        if min(kernel.shape) < INTERP_SIZE:
            x, y = np.unravel_index(activations.argmax(), activations.shape)
            convolution_params.append(x)
            convolution_params.append(y)
    return convolution_params


def add_dipole_label(common_desc, example):
    try:
        positions = np.array([[example.dip_posx, example.dip_posy, example.dip_posz]])
        # dipole_position_label = get_label_from_head_position_atlasreader(positions)[0]
        # dipole_position_label = get_label_from_head_position_freesurfer_parcelations(positions)[0]
        # common_desc['source'] = str(dipole_position_label)
        common_desc['dip_gof'] = example.dip_gof
        common_desc['dip_amplitude'] = example.dip_amplitude * 1000000000
    except:
        pass