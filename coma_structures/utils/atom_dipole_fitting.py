from tqdm import tqdm
import pandas as pd


def get_atoms(atoms, mp_params, width_coeff=1):
    columns = ['iteration', 'modulus', 'amplitude', 'width', 'frequency', 'phase', 'struct_len', 'absolute_position', 'offset',
               'ch_id', 'ch_name']
    dtypes = {'iteration': int, 'modulus': float, 'amplitude': float,
              'width': float, 'frequency': float, 'phase': float,
              'struct_len': float,
              'absolute_position': float,
              'offset': float,
              'ch_id': int,
              'ch_name': str,

    }

    chosen = []
    for atom_id, atom in enumerate(tqdm(atoms, desc='primary atom parametrisation')):
        channel = int(atom[0])
        iteration = atom[1]
        modulus = atom[2]
        amplitude = 2 * atom[3]
        position = atom[4]
        width = atom[5]
        frequency = atom[6]
        phase = atom[7]
        try:
            struct_len = width_coeff * width
        except:
            import IPython
            IPython.embed()

        offset = position - struct_len / 2
        mp_chnames = mp_params["channel_names"]
        mp_channel_id = channel - 1
        mp_channel_name = mp_chnames[mp_channel_id]

        chosen.append([iteration - 1, modulus,
                       amplitude, width, frequency, phase, struct_len, position, offset,
                       channel, mp_channel_name,
                       ])

    if not chosen:
        df = pd.DataFrame(columns=columns)
    else:
        df = pd.DataFrame(chosen, columns=columns)
    df = df.astype(dtype=dtypes)
    df = df.sort_values(['absolute_position', 'iteration', 'ch_id'])
    ch_num = len(mp_chnames)

    macroatom_id = []
    for i in range(int(df.shape[0] / ch_num)):
        macroatom_id.extend([i] * ch_num)
    df['macroatom_id'] = macroatom_id
    return df