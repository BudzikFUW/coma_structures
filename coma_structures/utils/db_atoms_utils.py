import os
import numpy as np
import sqlite3


def read_db_atoms(atom_db_path):
    if os.path.exists(atom_db_path) and atom_db_path.endswith('db'):
        cursor = sqlite3.connect(atom_db_path).cursor()
        # need to create a txt compatable numpy array
        # channel, iteration, modulus, amplitude (not p2p), position (absolute), width, frequency phase
        segment_length = float(cursor.execute(
            'SELECT segment_length_s FROM segments'
        ).fetchone()[0])
        atoms_db = cursor.execute(
            "SELECT channel_id, iteration, energy, amplitude, t0_s, scale_s, f_Hz, phase, segment_id FROM atoms where envelope='gauss'"
        )
        atoms = np.array(atoms_db.fetchall())
        # temporary: fix atoms offset
        atoms[:, 4] = atoms[:, 4] + segment_length * atoms[:, 8]
        atoms[:, 0] = atoms[:, 0] + 1
        atoms = atoms[:, 0:-1]
        return atoms
    else:
        print("Trying to read legacy txt decomposition")
        return np.loadtxt(os.path.splitext(atom_db_path)[0] + '.txt')
