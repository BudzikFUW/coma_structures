import glob
import json
import os
import pickle

from coma_structures.utils.db_atoms_utils import read_db_atoms

try:
    import pylab as pb
except:
    pass
import numpy as np
import argparse
import pandas as pd

from tqdm import tqdm

from coma_structures.research.generate_features import normalise_tags
from coma_structures.utils.interpolate_head_features import interpolate_feature, get_positions
from coma_structures.utils.snr_calculation import calculate_atom_neighborhood_snr

from coma_structures.utils.utils import (get_all_atoms, read_raw, preanalyse_raw, get_mp_book_paths,
                                         gabor, convolved_features)


def calculate_atom_normalised_amplitude(atoms):
    # std_per_channels.mean()  inb14 - "normal eeg"
    # Out[2]: 23.30182361322579
    sigma_of_exemplary_eeg = 23.30182361322579
    atoms['amplitude_normalized_to_textbook'] = atoms.amplitude_z_score * sigma_of_exemplary_eeg
    return atoms


def reconstruct(example_atoms, timeline):
    reconsctruction = np.zeros_like(timeline)

    for atom in example_atoms.itertuples():
        # atoms are loaded as p2p amplitudes
        atom_s = atom.amplitude / 2 * gabor(timeline,
                                        atom.width,
                                        atom.absolute_position,
                                        atom.frequency,
                                        atom.phase,
                                        ) * atom.atom_importance
        reconsctruction += atom_s
    return reconsctruction


def interpolate_feature_for_visualisation(atom_df, channels, interp_item='amplitude'):
    convolved_mask = np.zeros(len(atom_df.columns)).astype(bool)
    for nr, feature in enumerate(atom_df.columns):
        convolved_mask[nr] = feature in convolved_features

    convolve_examples = atom_df.values.T[convolved_mask].astype(np.float32).T[None, None, :]  # amp normalisation as expected
    pos = get_positions(tuple(channels))
    try:
        return interpolate_feature(convolve_examples, pos)[0, 0, :, :, convolved_features.index(interp_item)]
    except:
        import traceback
        traceback.print_exc()
        import IPython
        IPython.embed()

class AtomPicker:
    SEGMENT_LENGTH = 20
    # microvolt_per_sec = 281.6
    microvolt_per_sec = 50.6

    def __init__(self,  raws, atoms_l, tags_l, mp_params_l, files_to_work, mp_path, mp_type, montage, cache_folder):
        self.atoms_l = atoms_l
        self.tags_l = tags_l
        self.mp_params_l = mp_params_l
        self.mp_path = mp_path
        self.mp_type = mp_type
        self.montage = montage
        self.raws = raws
        self.files_to_work = files_to_work
        self.cache_folder = cache_folder
        self.channel_num = 10

        self.create_figure()

    def create_figure(self):
        fig = pb.figure()
        self.fig = fig
        self.scatter_ax = fig.add_axes([0.05, 0.3, 0.6, 0.60])
        self.reconstruction_ax = fig.add_axes([0.05, 0.05, 0.6, 0.25], sharex=self.scatter_ax)
        self.interpolation_ax = fig.add_axes([0.65, 0.3, 0.3, 0.35])
        fig.canvas.mpl_connect('button_release_event', self.onclick)
        fig.canvas.mpl_connect('key_release_event', self.on_button)
        fig.tight_layout()

    def onclick(self, event):

        if event.inaxes == self.scatter_ax:
            print('%s click: button=%d, x=%d, y=%d, xdata=%f, ydata=%f' %
                  ('double' if event.dblclick else 'single', event.button,
                   event.x, event.y, event.xdata, event.ydata))
            x, y = event.xdata, event.ydata
            xs = self.example_atoms['absolute_position']
            ys = self.example_atoms['frequency']
            distances = (((xs - x) ** 2 + (ys - y) ** 2) ** 0.5).values
            selected_atom_id = np.argmin(distances)
            self.render_atom_id = selected_atom_id
            important_atom_global_id = self.example_atoms.iloc[selected_atom_id].name
            if event.button == 1: # only change activeness when left
                if self.mp_type in ['mmp3', 'mmp1']:
                    # mark whole macroatom
                    macroatom_id = self.atoms_l[self.index[0]].loc[important_atom_global_id, 'mmp3_atom_ids']
                    macroatom_mask = self.atoms_l[self.index[0]]['mmp3_atom_ids'] == macroatom_id
                    new_importance_value = np.logical_not(self.atoms_l[self.index[0]].loc[important_atom_global_id, "atom_importance"])
                    self.atoms_l[self.index[0]].loc[macroatom_mask, 'atom_importance'] = new_importance_value
                else:
                    # mark only one channel
                    self.atoms_l[self.index[0]].loc[important_atom_global_id, "atom_importance"] = np.logical_not(self.atoms_l[self.index[0]].loc[important_atom_global_id, "atom_importance"])
                if self.atoms_l[self.index[0]].loc[important_atom_global_id, "atom_importance"]:
                    tag_name = self.tags_l[self.index[0]][self.index[1]]['description']['name']
                    if self.mp_type in ['mmp3', 'mmp1']:
                        macroatom_id = self.atoms_l[self.index[0]].loc[important_atom_global_id, 'mmp3_atom_ids']
                        macroatom_mask = self.atoms_l[self.index[0]]['mmp3_atom_ids'] == macroatom_id
                        self.atoms_l[self.index[0]].loc[macroatom_mask, 'label_manual_marking'] = tag_name
                    else:
                        self.atoms_l[self.index[0]].loc[important_atom_global_id, 'label_manual_marking'] = tag_name
                else:
                    if self.mp_type in ['mmp3', 'mmp1']:
                        macroatom_id = self.atoms_l[self.index[0]].loc[important_atom_global_id, 'mmp3_atom_ids']
                        macroatom_mask = self.atoms_l[self.index[0]]['mmp3_atom_ids'] == macroatom_id
                        self.atoms_l[self.index[0]].loc[macroatom_mask, 'label_manual_marking'] = "None"
                    else:
                        self.atoms_l[self.index[0]].loc[important_atom_global_id, 'label_manual_marking'] = "None"

            self.render()

    def save(self):
        print('Saving')
        for file_id in tqdm(range(len(self.files_to_work)), desc="Saving markings and full dataframes",
                                          total=len(self.files_to_work)):
            atoms = self.atoms_l[file_id]
            atoms_original_sort = atoms.sort_index()

            mp_filename = '{}_{}_{}.txt'.format(os.path.basename(self.files_to_work[file_id]),
                                                self.montage,
                                                self.mp_type)

            atom_importance_mask = atoms_original_sort.atom_importance.values
            np.savetxt(os.path.join(self.cache_folder, mp_filename), atom_importance_mask)

            manual_labels_filename = '{}_{}_{}_atom_labels_manual.txt.npy'.format(os.path.basename(self.files_to_work[file_id]),
                                                self.montage,
                                                self.mp_type)
            atoms_labels = atoms_original_sort.label_manual_marking.values
            np.save(os.path.join(self.cache_folder, manual_labels_filename), atoms_labels)

            full_atom_df = '{}_{}_{}_full_atom_dataframe.bz2'.format(
                os.path.basename(self.files_to_work[file_id]),
                self.montage,
                self.mp_type)
            atoms.to_pickle(os.path.join(self.cache_folder, full_atom_df))
        print('done')


    def on_button(self, event):
        print(event.key)
        if event.key == 'up':
            self.channel_num -= 1
            if self.channel_num < 0:
                self.channel_num = 0
            self.render()
        if event.key == 'down':
            self.channel_num += 1
            if self.channel_num >= len(self.raws[self.index[0]].ch_names):
                self.channel_num = len(self.raws[self.index[0]].ch_names) - 1
            self.render()
        if event.key == 'enter':
            self.save()

        if event.key == 'end':
            pass

    def get_mp_ch_id(self, channel_number):
        file_id = self.index[0]
        mp_params = self.mp_params_l[file_id]
        raw = self.raws[file_id]

        mp_channel = mp_params['channel_names'][channel_number]
        for raw_ch_id, raw_channel in enumerate(raw.ch_names):
            if mp_channel == raw_channel:
                return raw_ch_id

    def render(self):
        raw_now = self.raws[self.index[0]]
        filename = os.path.basename(self.files_to_work[self.index[0]])

        tag = self.tags_l[self.index[0]][self.index[1]]
        length = tag['duration']
        timeline_extention = self.SEGMENT_LENGTH - length
        if timeline_extention < 0:
            timeline_extention = 0
        
        
        # timeline = np.arange(self.start_stops_all[self.index][0] - timeline_extention / 2,
        #                      self.start_stops_all[self.index][1] + timeline_extention / 2,
        #                      1 / raw_now.info['sfreq'])
        time_start = tag['onset'] - timeline_extention / 2
        time_stop = tag['onset'] + length + timeline_extention / 2
        raw_index = raw_now.time_as_index([time_start, time_stop], use_rounding=True)
        signal, timeline = raw_now.get_data(start=raw_index[0], stop=raw_index[-1] + 1, return_times=True)
        try:
            signal = signal[self.channel_num, :] * 1e6
        except IndexError:
            self.channel_num = 0
            signal = signal[self.channel_num, :] * 1e6


        current_atoms_file = self.atoms_l[self.index[0]]
        tags_mask_time = (current_atoms_file.absolute_position >= tag['onset']) & (current_atoms_file.absolute_position <= (tag['onset'] + tag['duration']))
        current_atoms = current_atoms_file.loc[tags_mask_time]
        current_channel_atoms = current_atoms.loc[current_atoms.ch_id == (self.get_mp_ch_id(self.channel_num) + 1)]

        self.example_atoms = current_channel_atoms
        self.timeline = timeline
        self.signal = signal

        reconsctruction = reconstruct(self.example_atoms,
                                      self.timeline,)

        vertical_lines_prepare = timeline % self.SEGMENT_LENGTH
        vertical_lines = timeline[vertical_lines_prepare < 1.9 / raw_now.info['sfreq']]


        self.reconstruction_ax.clear()
        self.scatter_ax.clear()
        self.interpolation_ax.clear()
        for line in vertical_lines:
            self.reconstruction_ax.axvline(line, color='red', linewidth=0.5)
        self.reconstruction_ax.plot(self.timeline, reconsctruction, color='red', linewidth=0.5,)
        self.reconstruction_ax.plot(self.timeline, self.signal, color='blue', linewidth=0.5,)
        self.scatter_ax.scatter(self.example_atoms['absolute_position'],
                                self.example_atoms['frequency'],
                                s=self.example_atoms['modulus'],
                                c=self.example_atoms['atom_importance'],
                                alpha=0.6,
                                )
        scale = (self.timeline[-1] - self.timeline[0]) * self.microvolt_per_sec
        self.reconstruction_ax.set_ylim([-scale/2, scale/2])

        channel = raw_now.info['ch_names'][self.channel_num]
        segment = (tag['onset'] // self.SEGMENT_LENGTH) + 1
        time_in_segment = tag['onset'] % self.SEGMENT_LENGTH
        self.scatter_ax.set_title('{} {} {} \nsegment: {} + {}s'.format(tag['description']['name'],
                                                                      channel, filename,
                                                                      segment, time_in_segment))
        # mmp3 only
        amp_visualisation_atom = current_atoms.iloc[(self.render_atom_id*len(raw_now.info['ch_names'])):(self.render_atom_id*len(raw_now.info['ch_names'])) + len(raw_now.info['ch_names'])]
        try:
            datas = interpolate_feature_for_visualisation(amp_visualisation_atom,
                                                           raw_now.info['ch_names'])
            self.interpolation_ax.imshow(datas, origin='lower')
        except OSError:
            pass
        self.fig.canvas.draw()

        # saving of the important_atom_mask

    def pick_atoms(self, tag_examples_indices, balance=True):
        labels_all = set()
        all_labels_for_indices = []
        for tag_index in tag_examples_indices:
            tag = self.tags_l[tag_index[0]][tag_index[1]]
            labels_all.add(tag['description']['name'])
            all_labels_for_indices.append(tag['description']['name'])

        all_labels_for_indices = np.array(all_labels_for_indices)

        tag_examples_indices = np.array(tag_examples_indices)
        label_indices_balanced = []
        if balance:
            for label in labels_all:
                mask = (all_labels_for_indices == label)
                label_indices = tag_examples_indices[mask]
                label_indices_balanced.append(label_indices)
        else:
            label_indices_balanced.append(tag_examples_indices)

        for index_bal in zip(*label_indices_balanced):
            for index in index_bal:
                self.render_atom_id = 0
                self.index = index

                self.render()
                pb.show()
                self.create_figure()


def filter_atoms_by_iteration(atoms, max_iter=175):
    return atoms[atoms.iteration <= max_iter]


def get_atoms_with_primary_parametrisation(raw_file, tagfile, montage, mp_dir, mp_type, atom_signal_cache, max_iter=175, unique_file_names=True, resample=True, copy=False):
    raw = read_raw(raw_file, tagfile)
    preanalysed_raw = preanalyse_raw(raw, montage, resample, copy=copy)
    path_to_book, path_to_book_params = get_mp_book_paths(mp_dir, raw_file,
                                                                       montage, mp_type, unique_file_names=unique_file_names)

    mp_params = json.load(open(path_to_book_params))
    atoms = read_db_atoms(path_to_book)
    dir_to_load = os.path.dirname(atom_signal_cache)
    mp_filename = '{}_{}_{}.txt'.format(os.path.basename(raw_file),
                                        montage,
                                        mp_type)
    file_to_load = os.path.join(dir_to_load, mp_filename)
    atoms_importance_mask = np.zeros(atoms.shape[0], dtype=bool)
    if os.path.exists(file_to_load):
        mask_candidate = np.loadtxt(file_to_load).astype(np.bool)
        if mask_candidate.shape == atoms_importance_mask.shape:
            print('Using {} file to load atom importance'.format(file_to_load))
            atoms_importance_mask = mask_candidate

    labels_filename = '{}_{}_{}_atom_labels_manual.txt.npy'.format(os.path.basename(raw_file),
                                        montage,
                                        mp_type)
    file_to_load = os.path.join(dir_to_load, labels_filename)
    labels_manual = np.array(['unknown'] * atoms.shape[0], dtype=np.object)
    if os.path.exists(file_to_load):
        labels_manual_candidate = np.load(file_to_load, allow_pickle=True)
        if labels_manual_candidate.shape == atoms_importance_mask.shape:
            print('Using {} file to load atom manual labeling'.format(file_to_load))
            labels_manual = labels_manual_candidate
    atoms = get_all_atoms(atoms, preanalysed_raw, mp_params, atoms_importance_mask=atoms_importance_mask)

    dipole_path = os.path.join(os.path.dirname(path_to_book),
                             'dipole_fits',
                             os.path.basename(path_to_book) + '_dipole_fits.pickle')
    if os.path.exists(dipole_path):
        print("reading dipole fits")
        # assuming that dipole fits are in a pickle with exactly same ordering as atoms
        atoms_with_dipoles = pd.read_pickle(dipole_path)

        missing_columns = []
        for i in list(set(atoms_with_dipoles.columns) - set(atoms.columns)):
            if i.startswith('dip_'):
                missing_columns.append(i)
        missing_columns.sort()
        for missing_column in missing_columns:
            atoms[missing_column] = atoms_with_dipoles[missing_column]
    else:
        print("\n\n\n no dipole fits! \n\n\n")

    atoms = atoms.assign(label_manual_marking=labels_manual)
    atoms = atoms.sort_values(['absolute_position', 'iteration', 'ch_id'])

    atoms = calculate_atom_neighborhood_snr(atoms)
    atoms = calculate_atom_normalised_amplitude(atoms)
    # we have to drop it after calculating neighborhood
    atoms = filter_atoms_by_iteration(atoms, max_iter=max_iter)

    if mp_type in ['mmp3', 'mmp1']:
        mmp3_atom_ids = []
        for i in range(int(atoms.shape[0] / len(mp_params['channel_names']))):
            mmp3_atom_id = [i] * len(mp_params['channel_names'])
            mmp3_atom_ids.extend(mmp3_atom_id)
        atoms = atoms.assign(mmp3_atom_ids=mmp3_atom_ids)
    return atoms, raw, preanalysed_raw, mp_params


def tag_atoms(atoms, tags, mp_params):
    labels_from_tags = ['None'] * atoms.shape[0]
    mp_channels = mp_params['channel_names']
    for tag in tqdm(tags, desc='Marking atoms from tags'):
        label_from_tag = tag['name']
        start = tag['onset']
        stop = tag['onset'] + tag['duration']
        fitting_atoms = atoms.loc[(atoms['absolute_position'] >= start) & (atoms['absolute_position'] <= stop)]
        for atom_id in fitting_atoms.index.values:
            atom_ch_id = fitting_atoms.loc[atom_id].ch_id
            # need to check for channels or no channel tag?
            if tag['description']['channels']:
                ch_names_in_mp = set(mp_channels[int(atom_ch_id - 1)].split('-'))
                ch_names_in_tag = set(tag['referenceAsReadable'].split('-'))
                # if correct channel
                if len(ch_names_in_mp.intersection(ch_names_in_tag)):
                    if labels_from_tags[atom_id] == 'None':
                        labels_from_tags[atom_id] = label_from_tag
                    else:
                        labels_from_tags[atom_id] = ';'.join(list(set([labels_from_tags[atom_id], label_from_tag])))
            else:
                if labels_from_tags[atom_id] == 'None':
                    labels_from_tags[atom_id] = label_from_tag
                else:
                    labels_from_tags[atom_id] = ';'.join([labels_from_tags[atom_id], label_from_tag])

    atoms = atoms.assign(labels_from_tags=labels_from_tags)
    return atoms


def init_empty_manual_marking(atoms, mp_type='mmp3'):
    labels_manual = []
    if mp_type in ['mmp3', 'mmp1']:
        # macroatom - multichannel atom
        amount_of_macroatoms = atoms.iloc[-1].mmp3_atom_ids + 1
        channel_count = int(atoms.shape[0] / amount_of_macroatoms)
        assert (atoms.shape[0] / amount_of_macroatoms) == (atoms.shape[0] // amount_of_macroatoms)
        for i in tqdm(range(amount_of_macroatoms), total=amount_of_macroatoms, desc='init manual marking mmp3'):
            macroatom = atoms.loc[atoms.mmp3_atom_ids == i]
            # bool because .any() produces numpy.bool_ which cannot be checked using standard "is True"
            macroatom_important = bool(macroatom.atom_importance.any())

            macroatom_tag_labels_set = set(';'.join(macroatom.labels_from_tags).split(';'))
            amount_of_labels = len(macroatom_tag_labels_set)
            if amount_of_labels > 1 and 'None' in macroatom_tag_labels_set:
                macroatom_tag_labels_set.remove('None')
                amount_of_labels -= 1
            macroatom_tag_labels = ';'.join(macroatom_tag_labels_set)
            if macroatom_tag_labels == 'None':
                labels_manual.extend(['None']* channel_count)
                continue
            if (macroatom_important is True) and amount_of_labels == 1:
                labels_manual.extend([macroatom_tag_labels] * channel_count)
                continue
            if (macroatom_important is True) and amount_of_labels > 1:
                labels_manual.extend(['needs_redo'] * channel_count)
                continue
            if macroatom_important is False:
                labels_manual.extend(['unknown'] * channel_count)
                continue
            labels_manual.extend(['unknown'] * channel_count)

    else:
        for atom_importance, labels_from_tags in tqdm(atoms[['atom_importance', 'labels_from_tags']].values, desc='init manual marking'):
            if labels_from_tags == 'None':
                labels_manual.append('None')
                continue
            amount_of_labels = len(labels_from_tags.split(';'))
            if (atom_importance is True) and amount_of_labels == 1:

                labels_manual.append(labels_from_tags)
                continue
            if (atom_importance is True) and amount_of_labels > 1:
                labels_manual.append('needs_redo')
                continue

            if atom_importance is False:
                labels_manual.append('unknown')
                continue
            labels_manual.append('unknown')
    atoms = atoms.assign(label_manual_marking=labels_manual)
    return atoms


def get_tag_indices_needs_redoing(atoms_l, tags_l, mp_params_l):
    tag_indices_need_redo = []
    for file_nr, atoms, tags in zip(range(len(atoms_l)), atoms_l, tags_l):
        # atoms.loc[atoms['label_manual_marking'] == 'needs_redo']
        atoms_needs_redoing = atoms.loc[atoms['label_manual_marking'] == 'needs_redo']
        mp_channels = mp_params_l[file_nr]['channel_names']
        for position, ch_id in zip(atoms_needs_redoing['absolute_position'], atoms_needs_redoing['ch_id']):
            ch_names_in_mp = set(mp_channels[int(ch_id - 1)].split('-'))
            for tag_nr, tag in enumerate(tags):
                start = tag['onset']
                stop = tag['onset'] + tag['duration']
                if (position >= start) and (position <= stop):
                    # if correct tag channel
                    if tag['description']['channels']:
                        ch_names_in_tag = set(tag['referenceAsReadable'].split('-'))

                        if len(ch_names_in_mp.intersection(ch_names_in_tag)):
                            tag_indices_need_redo.append([file_nr, tag_nr])
                    # if tag is for all channels
                    else:
                        tag_indices_need_redo.append([file_nr, tag_nr])
    return tag_indices_need_redo


def init_known_none_tags(atoms):
    mask = (atoms['atom_importance'] == False) & (atoms['labels_from_tags'] == 'None') & (atoms['label_manual_marking'] == 'unknown')
    atoms.loc[mask, 'label_manual_marking'] = 'None'
    return atoms


def load_importance_mask_and_manual_labels(atoms, cache_path_labels, cache_path_importance):
    '''presumes loading from cache, and  already sorted'''
    sorting = atoms.index
    atoms_original_sort = atoms.sort_index()

    atom_importance = np.loadtxt(cache_path_importance).astype(bool)
    atoms_original_sort = atoms_original_sort.assign(atom_importance=atom_importance)

    atom_manual_labels = np.load(cache_path_labels, allow_pickle=True)
    atoms_original_sort = atoms_original_sort.assign(label_manual_marking=atom_manual_labels)

    return atoms_original_sort.iloc[sorting]


def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Analysing MP decompositions.")
    parser.add_argument('-d', '--mp-dir', nargs='?', type=str, help='MP Output directory')
    parser.add_argument('-m', '--montage', action='store', help='Montages: none, ears, transverse', default='none')
    parser.add_argument('-n', '--n-atoms-per-example', action='store', type=int, help='amount of atoms in example for no tags', default=20)
    parser.add_argument('-mp', '--mp-type', action='store', help='mp type: smp, mmp3', default='smp')
    parser.add_argument('-c', '--atom-signal-cache', action='store', type=str)
    parser.add_argument('files', nargs='+', metavar='file', help="path to *.raw")

    namespace = parser.parse_args()
    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files
    files_to_work = list(sorted(files_to_work))
    print(files_to_work)
    raws = []
    atoms_l = []
    tags_l = []
    mp_params_l = []

    for file_nr, file in tqdm(enumerate(files_to_work), total=len(files_to_work), desc='files'):
        tagfile = os.path.splitext(file)[0]+'_w.tag'

        dir_to_load = os.path.dirname(namespace.atom_signal_cache)
        cache = '{}_{}_{}_cache.pickle'.format(os.path.basename(file),
                                                           namespace.montage,
                                                           namespace.mp_type)
        cache_path = os.path.join(dir_to_load, cache)
        if os.path.exists(cache_path):
            print("Opening cache {}".format(cache_path))
            with open(cache_path, 'rb') as cache_file:
                atoms, raw, preanalysed_raw, mp_params = pickle.load(cache_file)

            dir_to_load = os.path.dirname(namespace.atom_signal_cache)
            cache_labels = '{}_{}_{}_atom_labels_manual.txt.npy'.format(os.path.basename(file),
                                                   namespace.montage,
                                                   namespace.mp_type)
            cache_importance = '{}_{}_{}.txt'.format(os.path.basename(file),
                                                                         namespace.montage,
                                                                         namespace.mp_type)
            cache_path_importance = os.path.join(dir_to_load, cache_importance)
            cache_path_labels = os.path.join(dir_to_load, cache_labels)
            if os.path.exists(cache_path_importance) and os.path.exists(cache_path_labels):
                atoms = load_importance_mask_and_manual_labels(atoms, cache_path_labels, cache_path_importance)
        else:
            atoms, raw, preanalysed_raw, mp_params = get_atoms_with_primary_parametrisation(file, tagfile, namespace.montage,
                                                                                 namespace.mp_dir, namespace.mp_type,
                                                                                 namespace.atom_signal_cache, max_iter=100000)
            with open(cache_path, 'wb') as cache_file:
                pickle.dump([atoms, raw, preanalysed_raw, mp_params], cache_file)

        tags = normalise_tags(raw)
        # We expect that every atom has a label, one label
        # Atoms be described by multiple tags from medical proffestional (overlapping tags)
        # Additionally, atoms can have "sure" tags, which are selected by user, it will add a sureness flag
        dir_to_load = os.path.dirname(namespace.atom_signal_cache)
        mp_filename = '{}_{}_{}_label_from_tag.bz2'.format(os.path.basename(file),
                                            namespace.montage,
                                            namespace.mp_type)
        label_from_file_path = os.path.join(dir_to_load, mp_filename)
        if os.path.exists(label_from_file_path):
            mask = pd.read_pickle(label_from_file_path)
            atoms = atoms.assign(labels_from_tags=mask.values)
        else:
            atoms = tag_atoms(atoms, tags, mp_params)
            atoms['labels_from_tags'].to_pickle(label_from_file_path)

        #######################################################

        if 'label_manual_marking' in atoms:
            atoms = init_known_none_tags(atoms)
        else:
            atoms = init_empty_manual_marking(atoms)
        raws.append(preanalysed_raw)
        atoms_l.append(atoms)
        tags_l.append(tags)
        mp_params_l.append(mp_params)

    tag_examples_indices = []
    for file_nr, tags in enumerate(tags_l):
        for tag_nr, tag in enumerate(tags):
            tag_examples_indices.append((file_nr, tag_nr))
    tag_examples_indices = list(sorted(list(set(tag_examples_indices))))
    tag_indices_need_redo = get_tag_indices_needs_redoing(atoms_l, tags_l, mp_params_l)
    np.random.shuffle(tag_examples_indices)
    dir_to_save = os.path.dirname(namespace.atom_signal_cache)
    atom_picker = AtomPicker(raws, atoms_l, tags_l, mp_params_l, files_to_work,
                             mp_path=namespace.mp_dir, mp_type=namespace.mp_type, montage=namespace.montage,
                             cache_folder=dir_to_save)

    try:
        atom_picker.pick_atoms(tag_examples_indices)
        atom_picker.save()
    except Exception as e:
        print("Atom picker crashed")
        import traceback
        traceback.print_exc()
        import IPython
        IPython.embed()








if __name__ == '__main__':
    main()
   # python mark_important_atoms.py -c D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_transverse_atom_picker\examples_for_importance.pickle -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp\ -m transverse -mp mmp3 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\*.raw
   # python mark_important_atoms.py -c D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_transverse_atom_picker_after_refactor\examples_for_importance.pickle -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp\ -m transverse -mp mmp3 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\*.raw
   # python mark_important_atoms.py -c D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_unipolar_after_refactor\examples_for_importance.pickle -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp\ -m none -mp mmp3 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\*.raw
   # python mark_important_atoms.py -c D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_unipolar_after_refactor_dipole_mmp1\examples_for_importance.pickle -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp\ -m none -mp mmp1 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\*.raw


    # fix ipython:
    # globals().update(locals())
