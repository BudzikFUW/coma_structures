import glob
import json
import os
import shutil
import sys

import mne
from math import ceil

import argparse

import numpy as np
import pandas
from tqdm import tqdm

from coma_structures.mark_important_atoms import \
    get_atoms_with_primary_parametrisation
from coma_structures.obci_readmanager.signal_processing.read_manager import \
    ReadManager
from coma_structures.obci_readmanager.signal_processing.signal.data_generic_write_proxy import \
    SamplePacket
from coma_structures.obci_readmanager.signal_processing.signal.data_raw_write_proxy import \
    DataRawWriteProxy
from coma_structures.obci_readmanager.signal_processing.signal.info_file_proxy import \
    InfoFileWriteProxy
from coma_structures.old.config import MP_EPOCH_IN_SECONDS, RESAMPLING_FREQUENCY
from coma_structures.utils.sanity_checking import \
    sanity_check_alpha
from coma_structures.utils.utils import get_mp_book_paths, gabor, gabor_envelope

SEGMENT_LENGTH = MP_EPOCH_IN_SECONDS
SAMPLING_FREQUENCY = RESAMPLING_FREQUENCY


def sanity_check_not_alpha_wrap(atoms):
    mask = []
    for atom_nr, atom in tqdm(atoms.iterrows(), desc='filtering atoms'):
        mask_item = sanity_check_alpha(atom, None)
        mask.append(mask_item)
    return atoms[np.logical_not(np.array(mask))]


def reconstruct(example_atoms, timeline, envelope=False):
    reconsctruction = np.zeros_like(timeline)

    for atom in example_atoms.itertuples():
        if envelope:
            # atoms amplitudes are loaded as p2p
            atom_s = atom.amplitude / 2 * gabor_envelope(timeline,
                                                atom.width,
                                                atom.absolute_position,
                                                atom.frequency,
                                                atom.phase,
                                                )
        else:
            # atoms amplitudes are loaded as p2p
            atom_s = atom.amplitude / 2 * gabor(timeline,
                                            atom.width,
                                            atom.absolute_position,
                                            atom.frequency,
                                            atom.phase,
                                            )
        reconsctruction += atom_s
    return reconsctruction


def get_outdir_path(outdir, raw_file, montage, mp_type, namespace):
    path_to_book, path_to_book_params = get_mp_book_paths(
        outdir, raw_file,
        montage, mp_type,
        unique_file_names=namespace.unique_file_names == 'y')

    raw_path = os.path.splitext(path_to_book)[0] + '.raw'
    info_path = os.path.splitext(path_to_book)[0] + '.xml'
    tag_file = os.path.splitext(path_to_book)[0] + '.tag'
    return raw_path, info_path, tag_file


def filter_atoms_by_dipole_params(atoms_in_segment, dip_gof=90.0, cortical_distance=2):
    gof_mask = atoms_in_segment['dip_gof'] >= dip_gof
    atoms_in_segment = atoms_in_segment[gof_mask]
    if isinstance(cortical_distance, (list, tuple)):
        distance_mask = np.logical_and(cortical_distance[0] <= atoms_in_segment['dip_distance_to_cortex_voxel'],
                                       atoms_in_segment['dip_distance_to_cortex_voxel'] <= cortical_distance[1]
                                       )
    else:
        distance_mask = np.abs(atoms_in_segment['dip_distance_to_cortex_voxel']) <= cortical_distance
    atoms_in_segment = atoms_in_segment[distance_mask]
    return atoms_in_segment


def parse_cortical_distance(distance_string):
    try:
        return float(distance_string)
    except:
        distance_list_strings = distance_string.split(';')
        return [float(i) for i in distance_list_strings]


def main():
    # np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Analysing MP decompositions.")
    parser.add_argument('-d', '--mp-dir', nargs='?', type=str, help='MP Output directory')

    parser.add_argument('-m', '--montage', action='store', help='Montages: none, ears, transverse', default='none')
    parser.add_argument('-mp', '--mp-type', action='store', help='mp type: smp, mmp3', default='smp')
    parser.add_argument('files', nargs='+', metavar='file', help="path to *.raw")
    parser.add_argument('-u', '--unique-file-names', action='store', type=str,
                        default='y',
                        help='are raw filenames unique? If no MP folder will contain subfolders')
    parser.add_argument('-o', '--outdir', nargs='?', type=str, help='Reconstructed signals output directory', )

    namespace = parser.parse_args()
    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files
    if namespace.mp_type not in ['mmp3', 'mmp1']:
        print("Unsupported mp type for dipole fitting")
        sys.exit(3)
    mp_dir = namespace.mp_dir
    montage = namespace.montage
    mp_type = namespace.mp_type
    outdir = namespace.outdir

    for raw_file in tqdm(files_to_work):
        path_to_book, path_to_book_params = get_mp_book_paths(
            mp_dir, raw_file,
            montage, mp_type,
            unique_file_names=namespace.unique_file_names == 'y')
        save_path = os.path.join(os.path.dirname(path_to_book),
                                 'dipole_fits',
                                 os.path.basename(
                                     path_to_book) + '_dipole_fits.pickle')
        try:
            atoms_original_sort = pandas.read_pickle(save_path)
        except FileNotFoundError:
            atoms_original_sort, raw, preanalysed_raw, mp_params = get_atoms_with_primary_parametrisation(
                raw_file, None,
                namespace.montage,
                namespace.mp_dir,
                namespace.mp_type,
                '', max_iter=175, unique_file_names=namespace.unique_file_names == 'y')

        end_point = np.max(atoms_original_sort['absolute_position'])

        mp_params = json.load(open(path_to_book_params))
        channels = mp_params['channel_names']

        segment_count = ceil(end_point / SEGMENT_LENGTH)
        amount_of_samples = int(segment_count * SEGMENT_LENGTH * SAMPLING_FREQUENCY)
        reconstruction = np.zeros((len(channels), amount_of_samples))

        for segment_nr in tqdm(list(range(segment_count)), desc='generating segments'):
            segment_t0 = segment_nr * SEGMENT_LENGTH
            segment_t1 = segment_nr * SEGMENT_LENGTH + SEGMENT_LENGTH
            segment_timeline = np.arange(segment_t0, segment_t1,
                                         1.0 / SAMPLING_FREQUENCY)
            mask_time = (atoms_original_sort[
                             'absolute_position'] >= segment_t0) * (
                                    atoms_original_sort[
                                        'absolute_position'] < segment_t1)
            segment_sample_start = int(segment_t0 * SAMPLING_FREQUENCY)
            segment_sample_end = int(segment_t1 * SAMPLING_FREQUENCY)
            atoms_in_segment = atoms_original_sort[mask_time]

            atoms_in_segment_filtered = sanity_check_not_alpha_wrap(atoms_in_segment)

            for channel_id, channel in enumerate(list(channels)):
                try:
                    channel_mask = atoms_in_segment_filtered['ch_name'] == channel
                except KeyError:
                    channel_mask = atoms_in_segment_filtered['ch_id'] == (channel_id + 1)
                atoms_in_channel = atoms_in_segment_filtered[channel_mask]
                channel_reconstruction = reconstruct(atoms_in_channel, segment_timeline, envelope=False)
                previous_sample_id = int(segment_nr * SEGMENT_LENGTH * SAMPLING_FREQUENCY - 1)
                previous_sample = reconstruction[channel_id, previous_sample_id]
                channel_reconstruction = channel_reconstruction - channel_reconstruction[0] + previous_sample
                reconstruction[channel_id, segment_sample_start:segment_sample_end] = channel_reconstruction

        info = mne.create_info(channels, sfreq=SAMPLING_FREQUENCY,
                               ch_types='eeg')
        raw = mne.io.RawArray(reconstruction*1e-6, info)
        rm = ReadManager.from_mne(raw)

        outfile_raw, outfile_info, tag_file_out = get_outdir_path(outdir, raw_file, montage, mp_type, namespace)

        outfile_raw = outfile_raw.replace('obci_mp', 'obci')
        outfile_info = outfile_info.replace('obci_mp', 'obci')
        tag_file_out = tag_file_out.replace('obci_mp', 'obci')

        data_proxy = DataRawWriteProxy(outfile_raw, False, rm.get_param('sample_type'))

        samples = rm.get_microvolt_samples().T

        for i, name in enumerate(rm.get_param('channels_names')):
            if name.startswith('Resp'):
                samples[:, i] *= 1e-6

        packet = SamplePacket(samples, np.linspace(0, 1, int(rm.get_param('number_of_samples'))))
        data_proxy.data_received(packet)
        data_proxy.finish_saving()
        info_proxy = InfoFileWriteProxy(outfile_info)
        params = rm.get_params()
        assert int(params['number_of_samples']) == samples.shape[0]
        info_proxy.finish_saving(params)
        for ending in ['_w.tag', '.tag']:
            tagfile = os.path.splitext(raw_file)[0] + ending
            # copy tags over
            try:
                shutil.copyfile(tagfile, tag_file_out)
            except:
                pass


if __name__=='__main__':
    main()
    # python reconstruct_signal_from_atoms_remove_alpha.py -u n -d /dmj/fizmed/mdovgialo/projekt_usuwanie_alfy_mp_plakat/dane_test/dane_N400_dla_M/MP/ -m none -mp mmp1 /dmj/fizmed/mdovgialo/projekt_usuwanie_alfy_mp_plakat/dane_test/dane_N400_dla_M/p1/N400_94247_od.obci.raw -o /dmj/fizmed/mdovgialo/projekt_usuwanie_alfy_mp_plakat/dane_test/dane_N400_dla_M/reconstructed_signals