import argparse
import shutil
from copy import copy

from coma_structures.obci_readmanager.signal_processing.tags.tags_file_reader import TagsFileReader
from coma_structures.obci_readmanager.signal_processing.tags import tags_file_writer as tags_writer
from numpy import genfromtxt


def select_and_correct_tags(tags_file, corrector_file):
    tags_reader = TagsFileReader(tags_file)
    tags = tags_reader.get_tags()
    filtered_tags = [i for i in tags if i['name'] != 'pryma']
    corrector = genfromtxt(corrector_file, delimiter=',')
    assert len(filtered_tags) == len(corrector)

    new_tags = []
    for tag, new_time in zip(filtered_tags, corrector):
        offset = tag['start_timestamp'] - new_time
        tag_new = copy(tag)
        tag_new['start_timestamp'] = new_time
        tag_new['end_timestamp'] = tag_new['end_timestamp'] - offset
        new_tags.append(tag_new)
    return new_tags


def write_tags(tags, file_path):
    writer = tags_writer.TagsFileWriter(file_path)
    for tag in tags:
        writer.tag_received(tag)
    writer.finish_saving(0.0)


def main():
    parser = argparse.ArgumentParser(description="Converting N400 slowa experiment tags, to be aligned with audio start, using pre corrected tag times csv")
    parser.add_argument('file_input', metavar='file', help="original tag file")
    parser.add_argument('file_corrector', metavar='file', help="tag exported from matlab with corrected timestamps")
    namespace = parser.parse_args()
    new_tags = select_and_correct_tags(namespace.file_input, namespace.file_corrector)
    shutil.copyfile(namespace.file_input, namespace.file_input + '.backup')
    write_tags(new_tags, namespace.file_input)


if __name__ == '__main__':
    main()