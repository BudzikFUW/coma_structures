import argparse
import glob
import sqlite3
import os
import numpy as np
from pandas import DataFrame
from scipy.io import savemat
from tqdm import tqdm


def read_db_atoms_df(atom_db_path):
    if os.path.exists(atom_db_path) and atom_db_path.endswith('db'):
        cursor = sqlite3.connect(atom_db_path).cursor()
        # need to create a txt compatable numpy array
        # channel, iteration, modulus, amplitude (not p2p), position (absolute), width, frequency phase
        segment_length = float(cursor.execute(
            'SELECT segment_length_s FROM segments'
        ).fetchone()[0])
        atoms_db = cursor.execute(
            "SELECT channel_id, iteration, energy, amplitude, t0_s, scale_s, f_Hz, phase, segment_id, t0_abs_s FROM atoms where envelope='gauss'"
        )
        atoms = np.array(atoms_db.fetchall())
        atoms = DataFrame(data=atoms, columns='channel_id,iteration,energy,amplitude,t0_s,scale_s,f_Hz,phase,segment_id,t0_abs_s'.split(','))

        fs = cursor.execute(
            "SELECT value FROM metadata where metadata.param='sampling_frequency_Hz'"
        ).fetchall()
        fs = float(fs[0][0])

        segment_length_samples = cursor.execute(
            "SELECT sample_count FROM segments"
        ).fetchone()[0]

        channel_count = cursor.execute(
            "SELECT value FROM metadata where metadata.param='channel_count'"
        ).fetchall()
        channel_count = int(channel_count[0][0])

        return atoms, fs, segment_length_samples, channel_count
    else:
        raise FileNotFoundError(atom_db_path)


def makemaps(atoms_df, freq_resolution, time_resolution, fs, segment_length_samples, channel_count, max_frequency):
    """Maps numpy array 4 dimensions: [channel count, segments, times, freqs]"""
    segments = sorted(np.unique(atoms_df['segment_id'].values))
    max_freq = min([fs/2, max_frequency])
    freqs = np.arange(0, max_freq, freq_resolution)
    times = np.arange(0, segment_length_samples/fs, time_resolution)

    maps = np.zeros((channel_count, len(segments), len(times), len(freqs)))
    for segment in tqdm(segments):
        segment_mask = atoms_df['segment_id'] == segment
        atoms_in_segment = atoms_df[segment_mask]
        for id, atom in atoms_in_segment.iterrows():
            time_envelope = np.exp(-np.pi * ((times - atom['t0_s']) / atom['scale_s']) ** 2)[None, :]
            freq_envelope = np.exp(-np.pi * ((freqs - atom['f_Hz']) * atom['scale_s']) ** 2)[:, None]
            atom_tf_envelope = np.kron(time_envelope, freq_envelope)
            atom_tf_envelope /= np.sum(atom_tf_envelope)
            atom_tf_envelope *= atom['energy']
            channel = int(atom['channel_id'])
            maps[channel, int(segment)] += atom_tf_envelope.T
    return maps, freqs, times


def draw_map(maps, freqs, times, channel=0, segment=0):
    import pylab
    xx, yy = np.meshgrid(times, freqs)
    pylab.figure()
    pylab.pcolormesh(xx, yy, maps[channel, segment].T)

    pylab.figure()
    pylab.pcolormesh(xx, yy, np.sqrt(maps[channel, segment]).T)

    pylab.show()


def draw_erds(maps, freqs, times, channel=0, baseline=(0.15, 0.22)):
    import pylab
    erds = np.mean(maps, axis=1)[channel]
    erds_balanced = erds.copy()
    mask = ((times > baseline[0]) * (times < baseline[1]))[0, :]
    for nr, line in enumerate(erds_balanced.T):
        erds_balanced[:, nr] = (line / line[mask].mean()*100-100)
    xx, yy = np.meshgrid(times, freqs)
    pylab.figure()
    pylab.pcolormesh(xx, yy, erds_balanced.T, vmax=50, vmin=-50, cmap='bwr')
    pylab.axvline(baseline[0])
    pylab.axvline(baseline[1])
    pylab.colorbar()
    pylab.figure()
    pylab.pcolormesh(xx, yy, erds.T)
    pylab.axvline(baseline[0])
    pylab.axvline(baseline[1])
    pylab.colorbar()
    pylab.show()


def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Make numpy maps")
    parser.add_argument('-o', '--output',  action='store', help='maps output npy', default='')
    parser.add_argument('-f', '--freq-resolution', type=float, action='store', help='Resolution of maps in frequencies', default=0.1)
    parser.add_argument('-mf', '--max-frequency', type=float, action='store', help='Resolution of maps map freq', default=30.0)
    parser.add_argument('-t', '--time-resolution', type=float, action='store', help='Resolution of maps in time', default=0.001)
    parser.add_argument('-m', '--matlab',  action='store', help='save as matlab file', default='n')
    parser.add_argument('files', nargs='+', metavar='file', help="path to *.db")

    namespace = parser.parse_args()
    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files
    print(files_to_work)
    for file in files_to_work:
        atoms_df, fs, segment_length_samples, channel_count = read_db_atoms_df(file)
        os.makedirs(namespace.output, exist_ok=True)

        maps, freqs, times = makemaps(atoms_df, namespace.freq_resolution, namespace.time_resolution, fs,
                        segment_length_samples, channel_count, namespace.max_frequency)

        basename = os.path.splitext(os.path.basename(file))[0]
        save_base_filename = os.path.join(namespace.output, basename)
        savedict = {'maps': maps, 'freqs': freqs, 'times': times}
        if namespace.matlab == 'y':
            savemat(save_base_filename + '.mat', savedict)
        else:
            np.savez(save_base_filename + '.npz',  maps=maps, freqs=freqs, times=times)






if __name__ == '__main__':
    main()
# python make_maps_per_mp_segment.py -f 0.2 -t 0.05 -mf 30.0 -o /dmj/fizmed/mdovgialo/projekt_erdrs_niemowlieta_mowa/dane/mp/none_mmp1/maps /dmj/fizmed/mdovgialo/projekt_erdrs_niemowlieta_mowa/dane/mp/none_mmp1/*.db