import pickle
from functools import partial
from time import strftime, gmtime
import pandas
from tensorflow import keras
import glob
import numpy as np
import argparse
from tqdm import tqdm
from tqdm.contrib.concurrent import process_map

from coma_structures.mark_important_atoms import get_atoms_with_primary_parametrisation
from coma_structures.research_important_atoms import split_to_macroatoms
from coma_structures.utils.data_preprocessing_utils import convert_montage_channel_nr_to_raw
from coma_structures.utils.multivariate_atom_analysis import get_kernels, parametrise_atom, add_dipole_label
from coma_structures.utils.neural_net_classify import convolve_data, second_level_parametrisation_predict_optimized
from coma_structures.obci_readmanager.signal_processing.tags import tags_file_writer as tags_writer
from coma_structures.utils.sanity_checking import atom_sanity_check
from coma_structures.utils.utils import TAG_DEFS


def _parametrize_macroatoms_internal(iter, kernels, raw):
    example, datas = iter
    datas = np.squeeze(datas)
    datas[np.logical_not(np.isfinite(datas))] = 0
    example_for_visualisation = parametrise_atom(example, raw.info['ch_names'], kernels, datas)
    return example_for_visualisation


def parametrise_atoms(macroatoms, examples_convolve, kernels, raw):
    fn = partial(_parametrize_macroatoms_internal, kernels=kernels, raw=raw)
    examples_nn = process_map(fn, list(zip(macroatoms, examples_convolve)), desc='parametrising atoms (convolve with kernels)', chunksize=512, max_workers=4)
    return pandas.DataFrame(examples_nn)


def create_macroatom_for_tag(nr, macroatom, examples_nn, label, common_desc, writable_pred='', writable_new_pred=''):
    macroatom.label_manual_marking = label
    macroatom['example_nr'] = nr
    macroatom['dip_atlas_location'] = common_desc['source']
    macroatom['prediction'] = writable_pred
    macroatom['prediction_2nd_level'] = writable_new_pred
    example = examples_nn.iloc[nr]
    assert macroatom.mmp3_atom_ids.values[0] == example.mmp3_atom_ids
    for column in set(example.axes[0]) - set(macroatom.columns):
        macroatom[column] = example[column]
    return macroatom



def make_tags_from_atoms(predictions, predictions_second_layer, macroatoms, examples_nn, preanalysed_raw, raw, sureness_only, sureness_threshold, mark_none):
    tags = []
    macroatoms_for_tags = []
    for nr in tqdm(range(len(predictions_second_layer)), desc='making tags', total=len(predictions_second_layer)):
        macroatom = macroatoms[nr]

        start = macroatom.iloc[0]["absolute_position"] - macroatom.iloc[0]["width"] / 2
        stop = macroatom.iloc[0]["absolute_position"] + macroatom.iloc[0]["width"] / 2

        only_use_sureness = (sureness_only == 'y')

        label, sureness, new_preds = atom_sanity_check(macroatom, predictions_second_layer[nr],
                                                       all_macroatoms=examples_nn,
                                                       all_macroatoms_preds=predictions_second_layer,
                                                       sureness_threshold=sureness_threshold,
                                                       only_use_sureness=only_use_sureness,
                                                       debug=False
                                                       )
        if label != 'None' or mark_none:
            max_amp = np.max(macroatom['amplitude'].values)
            frequency = macroatom['frequency'].values[0]
            amp_threshold = max_amp * 0.75
            amp_mask = (macroatom['amplitude'].values > amp_threshold)
            # channels where structure is strong enough to count as visible
            channels = np.arange(0, len(preanalysed_raw.info['ch_names']))[amp_mask]

            tag_on_all_channels_threshold = 0.8

            writable_pred = ', '.join(['{:.0f}'.format(ppp) for ppp in predictions[nr] * 100])
            writable_new_pred = ', '.join(['{:.0f}'.format(ppp) for ppp in predictions_second_layer[nr] * 100])
            # if more than 80% channels contain a structure it's an all channels structure



            common_desc = {'pred_raw': writable_pred,
                           'example_nr': str(nr),
                           'pred_2nd': writable_new_pred,
                           'frequency': str(frequency),
                           'amplitude_P2P': str(max_amp),
                           'width': str(macroatom['width'].values[0]),
                            }
            
            # don't add dipole labels to "nones" - takes to long
            if label != 'None':
                add_dipole_label(common_desc, examples_nn.loc[nr])
                macroatom_to_save_with_tags = create_macroatom_for_tag(nr, macroatom, examples_nn, label, common_desc, writable_pred, writable_new_pred)
                macroatoms_for_tags.append(macroatom_to_save_with_tags)

            if len(channels) >= len(preanalysed_raw.info['ch_names']) * tag_on_all_channels_threshold:
                desc = common_desc.copy()
                tag = {'channelNumber': -1,
                       'start_timestamp': start,
                       'end_timestamp': stop,
                       'name': label,
                       'desc': desc,
                       }
                tags.append(tag)
            else:
                for channel in channels:
                    desc = common_desc.copy()

                    channel_for_tag = convert_montage_channel_nr_to_raw(raw, preanalysed_raw, channel)
                    channel_montaged = preanalysed_raw.ch_names[channel]
                    desc['referenceAsReadable'] = str(channel_montaged)

                    tag = {'channelNumber': channel_for_tag,
                           'start_timestamp': start,
                           'end_timestamp': stop,
                           'name': label,
                           'desc': desc,
                           }
                    tags.append(tag)
    # tags should be sorted
    tags = list(sorted(tags, key=lambda x: float(x['start_timestamp'])))
    macroatoms_for_tags_df = pandas.concat(macroatoms_for_tags)
    return tags, macroatoms_for_tags_df


def create_parametrised_data(file, namespace):
    atoms, raw, preanalysed_raw, mp_params = get_atoms_with_primary_parametrisation(file, None,
                                                                                    namespace.montage,
                                                                                    namespace.mp_dir,
                                                                                    namespace.mp_type,
                                                                                    '', max_iter=175)

    macroatoms = split_to_macroatoms(atoms)

    kernels = get_kernels(namespace.montage)

    examples_convolve = convolve_data(macroatoms, mp_params)

    examples_nn = parametrise_atoms(macroatoms, examples_convolve, kernels, raw)
    return examples_nn, macroatoms, raw, preanalysed_raw


def read_cache_or_create_parametrised_and_save(file, namespace):
    file_base_name = os.path.basename(file)
    cachefile_examples_nn = file_base_name + '_first_level_feature_cache.bz2'
    cachefile_examples_nn_full_path = os.path.join(namespace.cache, cachefile_examples_nn)
    pickles_raws_path = os.path.join(namespace.cache,  file_base_name + '_raws.pickle')
    pickles_macroatoms_path = os.path.join(namespace.cache,  file_base_name + '_macroatoms.pickle')
    if namespace.cache and os.path.exists(cachefile_examples_nn_full_path):
        print("Reading from cache", namespace.cache)
        examples_nn = pandas.read_pickle(cachefile_examples_nn_full_path)
        with open(pickles_raws_path, 'rb') as pickes_raws_file:
            raw, preanalysed_raw = pickle.load(pickes_raws_file)
        with open(pickles_macroatoms_path, 'rb') as pickles_macroatoms_file:
            macroatoms = pickle.load(pickles_macroatoms_file)
        return examples_nn, macroatoms, raw, preanalysed_raw
    else:
        if namespace.cache:
            os.makedirs(namespace.cache, exist_ok=True)
        examples_nn, macroatoms, raw, preanalysed_raw = create_parametrised_data(file, namespace)
        if namespace.cache:
            examples_nn.to_pickle(cachefile_examples_nn_full_path)
            with open(pickles_raws_path, 'wb') as pickes_raws_file:
                pickle.dump([raw, preanalysed_raw], pickes_raws_file)
            with open(pickles_macroatoms_path, 'wb') as pickles_macroatoms_file:
                pickle.dump(macroatoms, pickles_macroatoms_file)
        return examples_nn, macroatoms, raw, preanalysed_raw




def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Mark_using nn")
    parser.add_argument('-d', '--mp-dir', nargs='?', type=str, help='MP Output directory')
    parser.add_argument('-m', '--montage', action='store', help='Montages: none, ears, transverse', default='none')
    parser.add_argument('-mp', '--mp-type', action='store', help='mp type: smp, mmp3', default='smp')
    parser.add_argument('-mdl', '--model', action='store',  help="nn_model .h5", default='')
    parser.add_argument('-o', '--output',  action='store', help='tags_output', default='')
    parser.add_argument('-s', '--sureness-only',  action='store', help='Only use surenes for sanity check', default='n')
    parser.add_argument('-n', '--mark-none',  action='store', help='Mark None tags', default='n')
    parser.add_argument('-t', '--sureness-threshold',  action='store', help='Sureness threshold for sanity check', default=0.3, type=float)
    parser.add_argument('-c', '--cache', action='store', help='features cache folder to accelerate testing', default='')

    parser.add_argument('files', nargs='+', metavar='file', help="path to *.raw")

    namespace = parser.parse_args()
    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files
    print(files_to_work)
    for file in files_to_work:
        model = keras.models.load_model(namespace.model)
        model_second_layer = keras.models.load_model(os.path.splitext(namespace.model)[0] + '_second_layer.h5')

        examples_nn, macroatoms, raw, preanalysed_raw = read_cache_or_create_parametrised_and_save(file, namespace)

        examples_nn = examples_nn.fillna(0)

        # dropping non features and reordering
        examples_nn_ready = examples_nn.drop(['labels_from_tags', 'file_id', 'mmp3_atom_ids', 'structure_name', 'atom_importance',
                                             'label_manual_marking', 'absolute_position', 'offset', 'ch_id', 'channel_name', 'phase_variability',
                                              'phase_variability_reversals', 'dip_closest_cortex_voxel_mni_x', 'dip_closest_cortex_voxel_mni_y', 'dip_closest_cortex_voxel_mni_z',
                              'dip_closest_cortex_voxel_label'], axis=1, errors='ignore')
        feature_columns = list(sorted(list(examples_nn_ready.columns.values)))
        examples_nn_ready = examples_nn_ready[feature_columns]

        # drop multichannel?
        # examples_nn_ready = drop_multichannel_features(examples_nn_ready)

        # TESTING REMOVING "HUMAN VISION" STUFF
        # examples_nn_ready.drop(
        #     ['iteration', 'snr'], axis=1, inplace=True)

        examples_nn_ready = examples_nn_ready.values
        print("Model, predicting")
        predictions = model.predict(examples_nn_ready)
        print("Model, predicting, done.")

        examples_seconds_level_nn_ready = second_level_parametrisation_predict_optimized(examples_nn, model, feature_columns, predictions)

        predictions_second_layer = model_second_layer.predict(examples_seconds_level_nn_ready)
        # import IPython
        # IPython.embed()
        # pb.hist(predictions[:, [0, 2, 3]].max(axis=-1), bins=50)
        # pb.show()

        sureness_threshold = namespace.sureness_threshold
        sureness_only = (namespace.sureness_only == 'y')

        # replace predictions_second_layer with predictions to remove context aware NN
        basename = os.path.splitext(os.path.basename(file))[0]
        if namespace.mark_none == 'y':
            tags, macroatoms_for_tags = make_tags_from_atoms(predictions, predictions_second_layer, macroatoms, examples_nn, preanalysed_raw, raw,
                                 sureness_only, sureness_threshold, True)
            none_dir = os.path.join(namespace.output, 'with_none', basename)
            os.makedirs(none_dir, exist_ok=True)

            tags_per_file = 10000
            tag_in_file = 0
            for tag in tags:
                if tag_in_file == 0:
                    start_time_f = float(tag.get('start_timestamp'))
                    if start_time_f <=0:
                        start_time_f = 0.5
                    start_time = strftime("%H-%M-%S", gmtime(start_time_f))
                    writer = tags_writer.TagsFileWriter(
                        os.path.join(none_dir, basename + '_with_none_{}.tag'.format(start_time)),
                        p_defs=TAG_DEFS)
                writer.tag_received(tag)
                tag_in_file += 1
                if tag_in_file == tags_per_file:
                    writer.finish_saving(0.0)
                    tag_in_file = 0

        # if true record both styles of tags
        tags, macroatoms_for_tags = make_tags_from_atoms(predictions, predictions_second_layer, macroatoms, examples_nn, preanalysed_raw,
                                    raw,
                                    sureness_only, sureness_threshold, False)
        os.makedirs(namespace.output, exist_ok=True)
        writer = tags_writer.TagsFileWriter(os.path.join(namespace.output,
                                                         os.path.splitext(os.path.basename(file))[0] + '.tag'),
                                            p_defs=TAG_DEFS)
        for tag in tags:
            writer.tag_received(tag)
        writer.finish_saving(0.0)

        print("Writing macroatoms per tags for {}".format(basename))
        macroatoms_output_dir_path = os.path.join(namespace.output, 'macroatoms')
        os.makedirs(macroatoms_output_dir_path, exist_ok=True)
        macroatoms_for_tags.to_pickle(os.path.join(macroatoms_output_dir_path, basename + '.bz2'))



if __name__ == '__main__':
    # turn off gpu for now
    import os
    os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
    main()

   # python mark_structures_nn.py -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp -m transverse -mp mmp3 -mdl D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_transverse_atom_picker_experiments\model_refactor.h5 -o D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\transverse_nn_tests_refactor D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\*.raw -s n -t 0.4
   # python mark_structures_nn.py -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp -m none -mp mmp3 -mdl D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_unipolar_after_refactor\model_refactor.h5 -o D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\none_nn_tests_refactor D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\*.raw -s n -t 0.4

   # python mark_structures_nn.py -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp -m transverse -mp mmp3 -mdl D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_transverse_atom_picker_after_refactor\model_refactor_test.h5 -o D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\none_nn_tests_refactor_snr -c D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\feature_cache -s n -t 0.4 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\*.raw
   # python mark_structures_nn.py -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\dane_snu_oznaczone\converted\mp -m transverse -mp mmp3 -mdl D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_transverse_atom_picker_after_refactor\model_refactor_test.h5 -o D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\dane_snu_oznaczone\converted\tags_experiments -c D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\dane_snu_oznaczone\converted\feature_cache -s n -t 0.4 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\dane_snu_oznaczone\converted\*.raw

   # python mark_structures_nn.py -d /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_stare_do_testu/converted/mp/ -m transverse -mp mmp3 -mdl  /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_stare_do_testu/converted/nn/model_refactor_test.h5 -o  /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_stare_do_testu/converted/tags_nn_context_broad -s n -t 0.4 /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_stare_do_testu/converted/*.raw
   # python mark_structures_nn.py -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp -m none -mp mmp3 -mdl D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_unipolar_after_refactor_dipole\model_refactor_test.h5 -o D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\tags_experiments\tags_dipoles_dla_Ani_avr -c D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\feature_cache_dip_avr D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\*.raw -s n -t 0.6 -n y

