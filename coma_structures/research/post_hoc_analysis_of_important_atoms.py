from collections import defaultdict

import argparse

import pandas as pd
import numpy as np
import pylab as pb
from sklearn.cluster import KMeans

from coma_structures.utils.utils import classes_d, gabor

segment_length = 20
timeline = np.arange(0, segment_length, 1/128)


def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Train NN on examples")
    parser.add_argument('file', metavar='file', help="pandas csv with hand picked example atoms")
    # parser.add_argument('-o', '--output', action='store',  help="nn_model .h5", default = '')

    namespace = parser.parse_args()

    examples = pd.read_csv(namespace.file, index_col=0, encoding='utf-8')

    classes = list(sorted(list(set(classes_d.keys()) - set(['None']))))

    reconstructions = defaultdict(list)
    params = defaultdict(list)
    kmeans_cluster = defaultdict(list)
    indexes = defaultdict(list)
    clusters = 2
    for index, example in examples.iterrows():
        if example.label != 'None':
            signal = gabor(timeline,
                  example.width,
                  10,
                  example.frequency,
                  example.phase
                  ) * example.amplitude / 2  # atoms are loaded as p2p amps
            reconstructions[example.label].append(signal)
            params[example.label].append([example.width, example.frequency, example.phase, example.amplitude])
            indexes[example.label].append(index)

    for class_ in params.keys():
        clusterizer = KMeans(n_clusters=clusters)
        kmeans_cluster[class_].extend(clusterizer.fit_predict(params[class_]))

    for class_ in reconstructions.keys():
        axs = []
        for i in range(clusters):
            fig = pb.figure()
            ax = fig.add_subplot(111)
            pb.title(class_ + ' cluster {}'.format(i))
            axs.append(ax)
        for reconstruction, cluster_id in zip(reconstructions[class_], kmeans_cluster[class_]):
            axs[cluster_id].plot(timeline, reconstruction)

    pb.show()

    examples.insert(len(examples.columns), 'cluster', -1, allow_duplicates=True)
    for class_ in reconstructions.keys():
        for nr, cluster_id in enumerate(kmeans_cluster[class_]):
            examples.at[indexes[class_][nr], 'cluster'] = cluster_id

    selected_clusters = defaultdict(list)
    for class_ in reconstructions.keys():
        print("Selected clusters for ", class_)
        print("numbers, comma seperated, -1 for all")
        while True:
            try:
                s = [int(i) for i in input().split(',')]
                print('got ', s)
                selected_clusters[class_].extend(s)
                break

            except:
                pass
    rows_to_drop = []
    for index, example in examples.iterrows():
        if example.label != 'None':
            if -1 in selected_clusters[example.label]:
                continue
            if example.cluster not in selected_clusters[example.label]:
                rows_to_drop.append(index)
    filtered = examples.drop(index=rows_to_drop)
    filtered = filtered.drop(columns=['cluster'] )
    filtered.to_csv(namespace.file + 'filtered_by_clusters.csv', encoding='utf-8')


if __name__ == '__main__':
    main()
    # python3 .\post_hoc_analysis_of_important_atoms.py D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_handpicked\manual_examples_dataframe.csv