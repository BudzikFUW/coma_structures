import argparse
import glob
import pylab as pb
from icecream import ic
import numpy as np
import pandas
from matplotlib.patches import Circle
from scipy.stats import mannwhitneyu

def main():
    # np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Analysing MP decompositions.")
    parser.add_argument('file', metavar='file',
                        help="pandas csv with hand picked example")

    namespace = parser.parse_args()
    file = namespace.file

    examples_df = pandas.read_csv(file, index_col=0)
    # import IPython
    # IPython.embed()
    spindle_mask = examples_df.label_manual_marking=='Spindle (wrzeciono)'
    spindles = examples_df[spindle_mask]

    nones = examples_df[examples_df.label_manual_marking=='None']

    # remove outliers
    spindles = spindles[spindles.frequency > 7]

    head_radius = 0.10  # m only for easy visualisation
    # MNE dipole brain space
    # x - right positive
    # y - forward positive
    # z - positive

    def anaylise(examples_to_vis, gof_thr=75):
        examples_to_vis = examples_to_vis[examples_to_vis.dip_gof > gof_thr]
        figure = pb.figure()
        ax = figure.add_subplot(111)
        draw_radii = np.array(examples_to_vis.dip_gof, dtype=float)
        examples_to_vis.plot.scatter(x='dip_posx', y='dip_posy', c='frequency', ax=ax, s=draw_radii, cmap='plasma', vmin=8, vmax=16, alpha=0.5)
        circle_patch = Circle((0, 0), head_radius, color='gray', alpha=0.1)
        ax.add_patch(circle_patch)
        ax.set_xlim([-head_radius, head_radius])
        ax.set_ylim([-head_radius, head_radius])

        figure2 = pb.figure()
        ax2 = figure2.add_subplot(111)

        pb.hist(examples_to_vis.dip_gof)

        if len(examples_to_vis) > 1:
            median = np.median(examples_to_vis.frequency)
            std = np.std(examples_to_vis.frequency)
            x_of_low = np.average(examples_to_vis[examples_to_vis.frequency <= median].dip_posx)
            y_of_low = np.average(examples_to_vis[examples_to_vis.frequency <= median].dip_posy)
            x_of_high = np.average(examples_to_vis[examples_to_vis.frequency > median].dip_posx)
            y_of_high = np.average(examples_to_vis[examples_to_vis.frequency > median].dip_posy)
            h, p_val_less = mannwhitneyu(examples_to_vis[examples_to_vis.frequency <= median].dip_posy,
                                 examples_to_vis[examples_to_vis.frequency > median].dip_posy,
                                    alternative='less'
                                )
            h, p_val_greater = mannwhitneyu(examples_to_vis[examples_to_vis.frequency <= median].dip_posy,
                                 examples_to_vis[examples_to_vis.frequency > median].dip_posy,
                                    alternative='greater'
                                )
            amount_of_examples = len(examples_to_vis)
            ic(median)
            ic(std)
            ic(amount_of_examples)
            ic(x_of_low, y_of_low)
            ic(x_of_high, y_of_high)
            ic(p_val_less)
            ic(p_val_greater)

    for file_id in np.unique(spindles.file_id):
        examples_to_vis = spindles[spindles.file_id==file_id]
        anaylise(examples_to_vis,75)


    anaylise(spindles, 75)
    anaylise(nones, 75)


    pb.show()


if __name__ == '__main__':
    main()

    # python spindle_dipole_analysis.py D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_unipolar_after_refactor_dipole\manual_examples_dataframe_test.csv
