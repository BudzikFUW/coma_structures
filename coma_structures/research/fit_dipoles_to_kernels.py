import os

import mne
import pandas
from mne.datasets import fetch_fsaverage

from coma_structures.research.show_example_dipole import show_example
from coma_structures.utils.interpolate_head_features import get_positions, \
    INTERP_SIZE
from coma_structures.utils.multivariate_atom_analysis import example_to_dipole, \
    get_kernels
import numpy as np


def fit_dipoles_to_kernels(debug=True):
    results = []
    fs_dir = fetch_fsaverage(verbose='CRITICAL')

    subjects_dir = os.path.join(fs_dir, '..')
    transform_path = os.path.join(fs_dir, 'bem', 'fsaverage-trans.fif')
    transform = mne.read_trans(transform_path, return_all=False, )

    channels = 'Fp1,Fp2,F3,F4,C3,C4,P3,P4,O1,O2,F7,F8,T3,T4,T5,T6,Fz,Cz,Pz'.split(
        ',')
    pos = get_positions(tuple(channels))

    kernels = get_kernels('none')


    # taken from  def interpolate_feature(data, pos): BE CAREFUL!
    res = INTERP_SIZE  # 16 is almost too much for memory usage...
    xmin = -1.5
    xmax = 1.5
    ymin = -1.4
    ymax = 1.6

    xi = np.linspace(xmin, xmax, res)
    yi = np.linspace(ymin, ymax, res)

    for kernel in kernels:
        kernel_img, kernel_name = kernel

        if kernel_img.shape != (res, res):
            new_kernel_img = np.zeros((res, res))
            x = int(np.abs((kernel_img.shape[0] - new_kernel_img.shape[0]) / 2))
            y = int(np.abs((kernel_img.shape[1] - new_kernel_img.shape[1]) / 2))
            new_kernel_img[x:x+kernel_img.shape[0], y:y+kernel_img.shape[1]] = kernel_img
            kernel_img = new_kernel_img

        example_amplitude = []
        example_phase = []
        example_width = []
        example_absolute_position = []
        example_frequency = []
        example_importance = []

        for channel_name, channel_pos in zip(channels, pos):
            x, y = channel_pos
            x_pixel = np.argmin(np.abs(xi - x))
            y_pixel = np.argmin(np.abs(yi - y))
            pixel_amp = kernel_img[y_pixel, x_pixel]
            example_amplitude.append(pixel_amp)
            example_phase.append(1)
            example_width.append(1)
            example_absolute_position.append(1)
            example_frequency.append(1)
            example_importance.append(1)

        example = pandas.DataFrame(data=np.array([example_phase, example_amplitude,
                                        example_width,
                                        example_absolute_position,
                                        example_frequency,
                                        example_importance]).T,
                         columns=['phase', 'amplitude', 'width',
                                  'absolute_position', 'frequency',
                                  'atom_importance', ],)


        dip = example_to_dipole(example, channels, fs_dir=fs_dir, ref_channel='average')

        macroatom = example.copy()
        example = example.iloc[np.argmax(macroatom.amplitude)]

        example['dip_posx'] = dip.pos[0][0]
        example['dip_posy'] = dip.pos[0][1]
        example['dip_posz'] = dip.pos[0][2]

        example['dip_orix'] = dip.ori[0][0]
        example['dip_oriy'] = dip.ori[0][1]
        example['dip_oriz'] = dip.ori[0][2]

        example['dip_amplitude'] = dip.amplitude[0]
        example['dip_gof'] = dip.gof[0]
        example['atom_importance'] = 1

        if debug:
            show_example(example, channels, subjects_dir, transform, macroatom)
        results.append(kernel_name + ' ' + str(dip.gof[0]))
    return results

if __name__ == '__main__':
    results = fit_dipoles_to_kernels(False)
    for result in results:
        print(','.join(result.split()))

