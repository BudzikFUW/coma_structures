import os

import mne
import nibabel
import pandas
from mne.datasets import fetch_fsaverage
import numpy as np

from coma_structures.research.parcellation_search import \
    get_label_from_head_position_atlasreader, \
    get_label_from_head_position_freesurfer_parcelations
from coma_structures.utils.mne_freesurf_labels import read_freesurfer_lut
import sys
import pylab as pb


dir = fetch_fsaverage()

subjects_dir = os.path.join(dir, '..')
transform_path = os.path.join(dir, 'bem', 'fsaverage-trans.fif')
transform = mne.read_trans(transform_path, return_all=False, )
lut_inv_dict = read_freesurfer_lut()[0]
label_lut = {v: k for k, v in lut_inv_dict.items()}
parcelation_fsaverage_img = nibabel.load(os.path.join(subjects_dir, 'fsaverage', 'mri', 'aparc.a2005s+aseg.mgz'))

file = sys.argv[-1]
pd = pandas.read_csv(file)

for name in np.unique(pd['label_manual_marking']):
    if name != 'None':
        mask = pd['label_manual_marking'] == name
        posx, posy, posz = pd[mask][['dip_posx', 'dip_posy', 'dip_posz']].mean()

        dip = mne.Dipole([0], [[posx, posy, posz]], [1, ], [[0, 0, 0]], [100.],
                         name=None, conf=None, khi2=None, nfree=None,
                         verbose=None)
        fig = dip.plot_locations(transform, 'fsaverage', subjects_dir,
                                 show=False, block=False)
        ax = pb.gca()
        ax.set_title(name)
        pb.suptitle('')


        print('\n\n name {}'.format(name))
        print([posx, posy, posz])
        title = 'AAL: {}\n fsav: {} \n h&o: {}'.format(
            get_label_from_head_position_atlasreader([[posx, posy, posz]], atlas='aal')[0],
            get_label_from_head_position_freesurfer_parcelations([[posx, posy, posz]])[0],
            get_label_from_head_position_atlasreader([[posx, posy, posz]], atlas='harvard_oxford')[0]
            )
        print(title)

pb.show()

import IPython
IPython.embed()

# spindles:
# https://www.frontiersin.org/files/Articles/461196/fnins-13-00727-HTML/image_m/fnins-13-00727-t001.jpg
# wzgórze/
# supplemental motor area

# k-complex?
# centrum?

# vertex sharp?
#


#  name Kompleks K
# [0.0024252103820732902, -0.002931163918627026, 0.07660861911409328]
# AAL: Cingulate_Mid_R
#  fsav: ctx-rh-S_subparietal
#  h&o: [92.0, 'Right_Cingulate_Gyrus_posterior_division']
#
#
#  name Spindle (wrzeciono)
# [-0.003142974982585871, 0.03321813933824179, 0.07466962480072538]
# AAL: Cingulate_Mid_L
#  fsav: Unknown
#  h&o: [96.0, 'Left_Cingulate_Gyrus_anterior_division']
#
#
#  name Wierzchołkowa fala ostra
# [0.005206022245592502, 0.01795458389567986, 0.08774658147289283]
# AAL: Cingulate_Mid_R
#  fsav: ctx-rh-S_cingulate-Main_part_and_Intracingulate
#  h&o: [36.0, 'Right_Precentral_Gyrus']