import os
import sys

import argparse
import mne

import numpy as np
import pandas
import pylab as pb
from mne.datasets import fetch_fsaverage

from coma_structures.mark_important_atoms import reconstruct, interpolate_feature_for_visualisation
from coma_structures.research.parcellation_search import get_label_from_head_position_atlasreader, \
    get_label_from_head_position_freesurfer_parcelations
from coma_structures.utils.multivariate_atom_analysis import amplitude_signs, example_to_dipole


def render(macroatom, ch_names, debug=True):
    SEGMENT_LENGTH = 20
    microvolt_per_sec = 50.6

    abs_pos = macroatom.absolute_position.values[0]

    timeline = np.arange(abs_pos - SEGMENT_LENGTH / 2, abs_pos + SEGMENT_LENGTH / 2, 0.001)

    for_reconstr = macroatom.loc[macroatom.amplitude.idxmax()].to_frame().transpose()
    for_reconstr.atom_importance = True
    reconsctruction = reconstruct(for_reconstr,
                                  timeline, )

    signs = amplitude_signs(macroatom, debug=debug)

    fig = pb.figure()
    reconstruction_ax = fig.add_axes([0.05, 0.05, 0.9, 0.25])
    interpolation_ax = fig.add_axes([0.05, 0.3, 0.90, 0.7])
    reconstruction_ax.plot(timeline, reconsctruction, color='red', linewidth=0.5, )
    scale = (timeline[-1] - timeline[0]) * microvolt_per_sec
    reconstruction_ax.set_ylim([-scale / 2, scale / 2])

    macroatom_for_vis = macroatom.copy()
    macroatom_for_vis.amplitude = macroatom_for_vis.amplitude * signs

    try:

        datas = interpolate_feature_for_visualisation(macroatom_for_vis,
                                                      ch_names)
        vmin = -np.nanmax(np.abs(datas))
        vmax = np.nanmax(np.abs(datas))
        m = interpolation_ax.imshow(datas, origin='lower', cmap='coolwarm', vmin=vmin, vmax=vmax)
        pb.colorbar(m)
    except OSError:
        pass
    return fig


def show_example(example, ch_names, subjects_dir, transform, macroatom, show=True, debug=True):
    dipole_pos = [example['dip_posx'], example['dip_posy'], example['dip_posz']]
    dipole_ori = [example['dip_orix'], example['dip_oriy'], example['dip_oriz']]
    gof = example['dip_gof']
    try:
        amplitude = example['dip_amplitude']
    except KeyError:
        amplitude = 1.0
    dip = mne.Dipole([0], [dipole_pos], [amplitude, ], [dipole_ori], [gof],
                     name=None, conf=None, khi2=None, nfree=None, verbose=None)

    fig_render = render(macroatom, ch_names, debug=debug)

    fig = dip.plot_locations(transform, 'fsaverage', subjects_dir, show=False)
    ax = pb.gca()
    title = 'AAL: {}\n fsav: {} \n h&o: {}'.format(
        get_label_from_head_position_atlasreader([dipole_pos], atlas='aal')[0],
        get_label_from_head_position_freesurfer_parcelations([dipole_pos])[0],
        get_label_from_head_position_atlasreader([dipole_pos],
                                                 atlas='harvard_oxford')[0]
    )
    print(example)
    print(title)
    ax.set_title("From file (with reference)")
    fig.tight_layout()

    # dip_no_ref = example_to_dipole(macroatoms[number], channels=ch_names, ref_channel='', fs_dir=os.path.join(subjects_dir, 'fsaverage'))
    # dip_no_ref.plot_locations(transform, 'fsaverage', subjects_dir, show=False)
    # ax = pb.gca()
    # ax.set_title("avg reference")
    #
    #
    # title = 'No reference (average):' \
    #         '\n' \
    #         'AAL: {}\n fsav: {} \n h&o: {}'.format(
    #     get_label_from_head_position_atlasreader(dip_no_ref.pos, atlas='aal')[0],
    #     get_label_from_head_position_freesurfer_parcelations(dip_no_ref.pos)[0],
    #     get_label_from_head_position_atlasreader(dip_no_ref.pos, atlas='harvard_oxford')[0]
    #     )
    # print(title)
    if show:
        pb.show()
    return fig_render, fig

def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Show position and label of a dipole")
    parser.add_argument('file', metavar='file',
                        help="example cache file")
    parser.add_argument('number', action='store', help="example number", type=int)

    print(parser)
    namespace = parser.parse_args()

    examples = pandas.read_pickle(namespace.file)
    macroatoms_file = namespace.file[:-30] + '_macroatoms.pickle'
    macroatoms = pandas.read_pickle(macroatoms_file)
    raws_file = namespace.file[:-30] + '_raws.pickle'
    raw, preanalysed_raw = pandas.read_pickle(raws_file)
    ch_names = preanalysed_raw.ch_names
    number = namespace.number

    dir = fetch_fsaverage()
    subjects_dir = os.path.join(dir, '..')
    transform_path = os.path.join(dir, 'bem', 'fsaverage-trans.fif')
    transform = mne.read_trans(transform_path, return_all=False, )

    while True:
        example = examples.loc[number]
        macroatom = macroatoms[number]

        show_example(example, ch_names, subjects_dir, transform, macroatom)

        while True:
            try:
                number = int(input("Next example number?\n"))
                break
            except KeyboardInterrupt:
                sys.exit()
            except:
                pass


if __name__ == '__main__':
    main()