import glob
import json
import os

import numpy as np
import argparse

from coma_structures.research.generate_features import normalise_tags, examples_for_tags
from coma_structures.utils.db_atoms_utils import read_db_atoms

from coma_structures.utils.utils import get_all_atoms, read_raw, preanalyse_raw, get_mp_book_paths, ATOM_FEATURES


def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Analysing MP decompositions.")
    parser.add_argument('-d', '--mp-dir', nargs='?', type=str, help='MP Output directory')
    parser.add_argument('-m', '--montage', action='store', help='Montages: none, ears, transverse', default='none')
    parser.add_argument('-n', '--n-atoms-per-example', action='store', type=int, help='amount of atoms in example', default=50)
    parser.add_argument('-mp', '--mp-type', action='store', help='mp type: smp, mmp3', default='smp')
    parser.add_argument('files', nargs='+', metavar='file', help="path to *.raw")


    namespace = parser.parse_args()
    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files
    print(files_to_work)
    for file in files_to_work:
        tagfile = os.path.splitext(file)[0]+'_w.tag'

        raw = read_raw(file, tagfile)
        preanalysed_raw = preanalyse_raw(raw, namespace.montage)
        path_to_book, path_to_book_params = get_mp_book_paths(namespace.mp_dir, file,
                                                                           namespace.montage, namespace.mp_type)

        mp_params = json.load(open(path_to_book_params))
        atoms = read_db_atoms(path_to_book)
        atoms = get_all_atoms(atoms, preanalysed_raw, mp_params)

        tags = normalise_tags(raw)
        examples, classes, labels, start_stops = examples_for_tags(tags, atoms, mp_params, mp_type=namespace.mp_type,
                                                                   atoms_per_example=namespace.n_atoms_per_example)

        try:
            examples_all = np.concatenate([examples_all, examples[:, :, 0:17, :]])
            classes_all = np.concatenate([classes_all, classes])
            labels_all.extend(labels)
            start_stops_all = np.concatenate([start_stops_all, start_stops])
        except NameError:
            examples_all = examples[:, :, 0:17, :]
            classes_all = classes
            labels_all = labels
            start_stops_all = start_stops

    # histogram ważony energią

    import pylab as pb

    for feature_label in ['phase', 'frequency', 'width', 'amplitude_z_score']:

        for label in np.unique(labels_all):
            pb.figure()
            mask = (np.array(labels_all)==label)
            bg_mask = (np.array(labels_all)=='None')
            bg_feature = examples_all[bg_mask, :, :, ATOM_FEATURES.index(feature_label)].flatten()
            bg_weights = examples_all[bg_mask, :, :, ATOM_FEATURES.index('modulus')].flatten()**2

            feature = examples_all[mask, :, :, ATOM_FEATURES.index(feature_label)].flatten()
            weights = examples_all[mask, :, :, ATOM_FEATURES.index('modulus')].flatten()**2
            pb.hist([feature, bg_feature], weights=[weights, bg_weights], bins=100, normed=True)
            pb.title(' '.join([label, feature_label]))
            pb.savefig('img/{}_{}.png'.format(label, feature_label))

    pb.show()


    import IPython
    IPython.embed()






if __name__ == '__main__':
    main()
   # python research_features.py -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp\ -m none -mp mmp3 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\e1002368.raw


