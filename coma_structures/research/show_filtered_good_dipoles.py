import glob
import os
import sys

import argparse
import mne

import numpy as np
import pandas
import pylab
from mne.datasets import fetch_fsaverage
from tqdm import tqdm

from coma_structures.research.show_example_dipole import show_example


def show_dipole_locations(example, transform, subjects_dir):
    dipole_pos = np.array([example['dip_posx'], example['dip_posy'], example['dip_posz']]).T
    dipole_ori = np.array([example['dip_orix'], example['dip_oriy'], example['dip_oriz']]).T
    gof = example['dip_gof']

    try:
        amplitude = example['dip_amplitude']
    except KeyError:
        amplitude = 1.0

    dip = mne.Dipole([0] * len(gof), dipole_pos, amplitude, dipole_ori, gof,
                     name=None, conf=None, khi2=None, nfree=None, verbose=None)
    fig = dip.plot_locations(transform, 'fsaverage', subjects_dir, show=False)
    return fig

def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Show position and label of list of dipoles filtered")
    parser.add_argument('files', nargs='+', metavar='file', help="path to full_atom_dataframe.bz2")

    print(parser)
    namespace = parser.parse_args()

    atom_type = 'Spindle (wrzeciono)'
    atom_dip_gof = 79

    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files

    dir = fetch_fsaverage()
    subjects_dir = os.path.join(dir, '..')
    transform_path = os.path.join(dir, 'bem', 'fsaverage-trans.fif')
    transform = mne.read_trans(transform_path, return_all=False, )

    examples_all = None

    for file in files_to_work:

        examples = pandas.read_pickle(file)

        type_macroatom_ids = (examples[examples.label_manual_marking == atom_type].mmp3_atom_ids).unique()
        examples_type = examples[
            examples.mmp3_atom_ids.isin(type_macroatom_ids)]
        examples_filtered = examples_type[examples_type.dip_gof > atom_dip_gof]
        examples_filtered['file_id'] = os.path.basename(file)
        if examples_all is None:
            examples_all = examples_filtered
        else:
            examples_all = pandas.concat([examples_all, examples_filtered])

    dipoles_fig = show_dipole_locations(examples_all, transform, subjects_dir)
    historgram_fig = examples_all.hist(figsize=(20,20))

    pylab.savefig('img/hist.png')
    pylab.show()
    for group in tqdm(examples_all.groupby(by=['mmp3_atom_ids', 'file_id'])):
        mmp3_atom_id, macroatom = group
        # not fully example, but fine
        channel_names = macroatom.channel_name
        channel_num = len(channel_names)
        example = macroatom.loc[macroatom.amplitude.idxmax()]
        print(example.file_id)
        fig_render, fig_dipole = show_example(example, channel_names, subjects_dir, transform, macroatom, debug=False, show=False)
        try:
            fig_render.savefig('img/{}_{}_atom.png'.format(example.file_id, mmp3_atom_id))
            fig_dipole.savefig('img/{}_{}_dipole.png'.format(example.file_id, mmp3_atom_id))
        except:
            pass

if __name__ == '__main__':
    main()