import glob
import json
import os
from collections import defaultdict

import argparse
import numpy as np
import tqdm

from coma_structures.utils.db_atoms_utils import read_db_atoms
from coma_structures.utils.data_preprocessing_utils import prepare_data
from coma_structures.utils.utils import read_raw, get_mp_book_paths, get_all_atoms, preanalyse_raw, ATOM_FEATURES, \
    example_normalisation, classes_d


def normalise_reference_readable_bipolar(ref_str):
    """EEG P3-1*EEG O1"""
    ref_split = ref_str.split('-')
    new_ref_split = []
    for i in ref_split:
        new_ref_split.append(i.replace('EEG ', '').replace('1*', ''))
    return '-'.join(new_ref_split)


def examples_from_atom_pd_smp(atoms, stop_time, atoms_per_example, label, skip):
    '''Generates list of examples.
    if atoms_per_example > 0 then creates fits amount af atoms to "stop_time"
    if more, create overlapping examples of atoms_per_example length until stop time

    '''
    examples = []
    labels = []
    for channel in np.unique(atoms[:, ATOM_FEATURES.index('ch_id')]):
        channel_mask = atoms[:, ATOM_FEATURES.index('ch_id')] == channel
        atoms_in_channel = atoms[channel_mask]

        if atoms_per_example > 0:
            i = 0
            while True:
                try:
                    example = atoms_in_channel[i:i + atoms_per_example]
                except IndexError:
                    break
                if example.shape[0] == atoms_per_example:
                    examples.append(example)
                    labels.append(label)
                else:
                    break
                if example[-1, ATOM_FEATURES.index('absolute_position')] > stop_time:
                    break
                i += skip

        else:
            # look for the last atom before stop:
            max_atom_fitting_in_time = len(atoms_in_channel)
            for i in range(len(atoms_in_channel)):
                if atoms_in_channel[i, ATOM_FEATURES.index('absolute_position')] > stop_time:
                    max_atom_fitting_in_time = i
                    break
            if max_atom_fitting_in_time > 0:
                example = atoms_in_channel[:max_atom_fitting_in_time]
                examples.append(example)
                labels.append(label)
    return examples, labels


def examples_from_atom_pd_mmp(atoms, stop_time, atoms_per_example, label, skip):
    '''Generates list of examples.
    if atoms_per_example > 0 then creates fits amount af atoms to "stop_time"
    if more, create overlapping examples of atoms_per_example length until stop time

    '''
    # we assume atoms were sorted by: absolute time, iteration, channel
    examples = []
    labels = []
    channel_n = len(np.unique(atoms[:, ATOM_FEATURES.index('ch_id')]))
    if channel_n == 0:
        return examples, labels
    atoms_mmp = atoms.reshape((atoms.shape[0]//channel_n, channel_n, atoms.shape[1]))

    if atoms_per_example > 0:
        i = 0
        with tqdm.tqdm(total=len(atoms_mmp)) as pbar:
            while True:
                try:
                    example = atoms_mmp[i:i + atoms_per_example]
                except IndexError:
                    break
                if example.shape[0] == atoms_per_example:
                    examples.append(example)
                    labels.append(label)
                else:
                    break
                if example[-1, 0, ATOM_FEATURES.index('absolute_position')] > stop_time:
                    break
                i += skip
                pbar.update(skip)
    else:
        # look for the last atom before stop:
        max_atom_fitting_in_time = len(atoms_mmp)
        for i in range(len(atoms_mmp)):
            if atoms_mmp[i, 0, ATOM_FEATURES.index('absolute_position')] > stop_time:
                max_atom_fitting_in_time = i
                break
        if max_atom_fitting_in_time > 0:
            example = atoms_mmp[:max_atom_fitting_in_time]
            examples.append(example)
            labels.append(label)
    return examples, labels


def examples_for_tags(tags, atoms, mp_params, mp_type='smp', atoms_per_example=-1):
    """mp_type: smp or mmp1/3/3
    atoms per example - only is used for non tag examples, tag examples always have variable size
    """
    mp_channels = mp_params['channel_names']
    atoms = atoms.sort_values(['absolute_position', 'iteration', 'ch_id'])[ATOM_FEATURES].values.astype(
        np.float32)  # features
    atoms_T = atoms.T
    examples = []
    labels = []

    # tag
    for tag_nr, tag in tqdm.tqdm(enumerate(list(tags)), desc='examples', total=len(tags)):
        start = tag['onset']
        stop = tag['onset'] + tag['duration']
        time_mask = ((atoms_T[ATOM_FEATURES.index('absolute_position')] >= start) & (
                atoms_T[ATOM_FEATURES.index('absolute_position')] <= stop))

        time_fitting = atoms[time_mask]
        channel_fiting_mask = []
        if mp_type == 'smp':
            for atom_ch_id in time_fitting[:, ATOM_FEATURES.index('ch_id')]:
                if tag['description']['channels'] == '':
                    channel_fiting_mask.append(True)
                else:
                    ch_names_in_mp = set(mp_channels[int(atom_ch_id - 1)].split('-'))
                    ch_names_in_tag = set(tag['referenceAsReadable'].split('-'))
                    if len(ch_names_in_mp.intersection(ch_names_in_tag)):
                        channel_fiting_mask.append(True)
                    else:
                        channel_fiting_mask.append(False)
            fitting_atoms = time_fitting[channel_fiting_mask]
        else:
            # mmp examples use whole head
            fitting_atoms = time_fitting

        if mp_type == 'smp':
            examples_for_tag, labels_for_tag = examples_from_atom_pd_smp(fitting_atoms, stop_time=stop,
                                                                         atoms_per_example=-1, # tags have max time
                                                                         # and varible atoms amount
                                                                         label=tag['name'],
                                                                         skip=0)
        else:

            examples_for_tag, labels_for_tag = examples_from_atom_pd_mmp(fitting_atoms, stop_time=stop,
                                                                         atoms_per_example=-1,
                                                                         label=tag['name'],
                                                                         skip=0)
        examples.extend(examples_for_tag)
        labels.extend(labels_for_tag)

    counter = defaultdict(int)
    for i in labels:
        counter[i] += 1
    try:
        print("Example counts: {}".format(counter))
    except UnicodeEncodeError:
        pass

    max_amount_of_tagged_examples = max(counter.values())

    # nontag:
    tagged_mask = np.zeros(atoms.shape[0], dtype=bool)
    for tag_nr, tag in tqdm.tqdm(enumerate(list(tags)), desc='examples_no_tag', total=len(tags)):
        start = tag['onset']
        stop = tag['onset'] + tag['duration']
        time_mask = ((atoms[:, ATOM_FEATURES.index('absolute_position')] >= start) & (atoms[:, ATOM_FEATURES.index('absolute_position')] < stop))
        tagged_mask = np.logical_or(tagged_mask, time_mask)

    nontagged_mask = np.logical_not(tagged_mask)
    nontagged_atoms = atoms[nontagged_mask]
    stop = nontagged_atoms[-1, ATOM_FEATURES.index('absolute_position')]

    if mp_type == 'smp':
        examples_for_non_tag, labels_for_non_tag = examples_from_atom_pd_smp(nontagged_atoms, stop_time=stop,
                                                                     atoms_per_example=atoms_per_example,
                                                                     label='None',
                                                                             skip=atoms_per_example)
    else:
        examples_for_non_tag, labels_for_non_tag = examples_from_atom_pd_mmp(nontagged_atoms, stop_time=stop,
                                                                     atoms_per_example=atoms_per_example,
                                                                     label='None',
                                                                             skip=atoms_per_example)
    # we want for neural net to have some idea that this class is the most probable, but we don't need
    # all of the examples
    NONTAGGED_EXAMPLE_RATIO = 10
    skip = len(examples_for_non_tag) // max_amount_of_tagged_examples // NONTAGGED_EXAMPLE_RATIO

    if skip != 0:
        examples.extend(examples_for_non_tag[::skip])
        labels.extend(labels_for_non_tag[::skip])
    else:
        examples.extend(examples_for_non_tag)
        labels.extend(labels_for_non_tag)

    classes = np.empty((len(labels)))
    for i in range(len(labels)):
        classes[i] = classes_d[labels[i]]

    start_stops = np.empty((len(examples), 2))
    for nr, example in enumerate(examples):
        if len(example.shape) == 3:
            start = example[0, 0, ATOM_FEATURES.index("absolute_position")] - example[0, 0, ATOM_FEATURES.index("width")]
            stop = example[-1, 0, ATOM_FEATURES.index("absolute_position")] + example[-1, 0, ATOM_FEATURES.index("width")]
        else:
            start = example[0, ATOM_FEATURES.index("absolute_position")] - example[0, ATOM_FEATURES.index("width")]
            stop = example[-1, ATOM_FEATURES.index("absolute_position")] + example[-1, ATOM_FEATURES.index("width")]
        start_stops[nr, 0] = start
        start_stops[nr, 1] = stop
    start_stops[start_stops < 0] = 0
    return examples, classes, labels, start_stops


def get_unmarked_atoms(atoms, atoms_classes):
    remove_ids = []
    for key in atoms_classes.keys():
        remove_ids.extend(list(atoms_classes[key].index))
    return atoms.drop(index=remove_ids)


def normalise_tags(raw):
    tags = []
    for tag in raw.annotations:
        tag['description'] = json.loads(tag['description'])
        tag['name'] = tag['description']['name']
        tags.append(tag)
        try:
            ref_readable_normal = normalise_reference_readable_bipolar(
                tag['description']['desc']['referenceAsReadable']
            )
            tag['channels_in_ref'] = ref_readable_normal.split('-')
            tag['referenceAsReadable'] = ref_readable_normal
        except KeyError as e:
            pass
    return tags


def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Analysing MP decompositions. Generates examples based on tags")
    parser.add_argument('-d', '--mp-dir', nargs='?', type=str, help='MP Output directory')
    parser.add_argument('-m', '--montage', action='store', help='Montages: none, ears, transverse', default='none')
    parser.add_argument('-n', '--n-atoms-per-example', action='store', type=int, help='amount of atoms in example', default=50)
    parser.add_argument('-mp', '--mp-type', action='store', help='mp type: smp, mmp3', default='smp')
    parser.add_argument('-o', '--output',  action='store', help='Examples_output', default='')
    parser.add_argument('files', nargs='+', metavar='file', help="path to *.raw")

    namespace = parser.parse_args()
    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files
    print(files_to_work)
    for file in files_to_work:
        tagfile = os.path.splitext(file)[0]+'_w.tag'

        raw = read_raw(file, tagfile)
        preanalysed_raw = preanalyse_raw(raw, namespace.montage)
        path_to_book, path_to_book_params = get_mp_book_paths(namespace.mp_dir, file,
                                                                           namespace.montage, namespace.mp_type)

        mp_params = json.load(open(path_to_book_params))
        atoms = read_db_atoms(path_to_book)
        atoms = get_all_atoms(atoms, preanalysed_raw, mp_params)

        tags = normalise_tags(raw)
        examples, classes, labels, start_stops = examples_for_tags(tags, atoms, mp_params, mp_type=namespace.mp_type,
                                                                   atoms_per_example=namespace.n_atoms_per_example)
        examples_norm = example_normalisation(examples, mp_params)
        if namespace.mp_type != 'smp':
            examples_norm, examples_convolve = prepare_data(examples_norm, mp_params)

        basename = os.path.splitext(os.path.basename(file))[0]
        if namespace.output:
            out_dir = namespace.output + '_{}'.format(namespace.n_atoms_per_example)
            os.makedirs(out_dir, exist_ok=True)
            from compress_pickle import dump, load
            if namespace.mp_type == 'smp':
                dump([examples_norm, classes, labels, start_stops], os.path.join(out_dir, basename+'.pickle'), compression='gzip')
            else:
                dump([examples_norm, examples_convolve, classes, labels, start_stops], os.path.join(out_dir, basename+'.pickle'), compression='gzip')


        # import IPython
        # IPython.embed()
        #
        # predictions = visualise_examples(examples_norm, labels)
        #
        # writer = TagsFileWriter('test.tag')
        # for start_stops, label in zip(start_stops, labels):
        #     tag_ch_id = - 1
        #     if label == 1:
        #         continue
        #     tag = {'channelNumber': tag_ch_id,
        #            'start_timestamp': start_stops[0],
        #            'end_timestamp': start_stops[1],
        #            'name': 'test_tag',
        #            'desc': {}}
        #     length = start_stops[1] - start_stops[0]
        #     if label is not None and length < 5:
        #             writer.tag_received(tag)
        # writer.finish_saving(0.0)


        # aucs = []
        # for n in range(1, 100, 4):
        #     examples, classes, labels, start_stops = examples_for_tags(tags, atoms, mp_params,
        #                                                                mp_type=namespace.mp_type, atoms_per_example=n)
        #     examples_norm = example_normalisation(examples)
        #     auc = stupid_auc_calc(examples_norm, labels)
        #     aucs.append(auc)
        # import pylab as pb
        # pb.plot(list(range(1,20)), aucs)
        # pb.show()
        # import IPython
        # IPython.embed()
        # visualise_examples(atoms_examples)
        # visualise_atoms(atom_classes)

        # atoms_all, atoms_all_start_stop = create_examples_per_channel({None: atoms})
        #
        # train_on_atoms(atoms_examples, atoms_examples_start_stop, atoms_all, atoms_all_start_stop, raw, mp_params)


if __name__ == '__main__':
    main()

    #python generate_features.py -o D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\features_50_transverse_mmp3\ -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp\ -m transverse -mp mmp3 D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\*.raw
