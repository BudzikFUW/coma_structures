import json
import os
import time

import mne
import nibabel
import numpy as np
from mne.datasets import fetch_fsaverage
from atlasreader.atlasreader import read_atlas_peak

from coma_structures.utils.mne_freesurf_labels import read_freesurfer_lut

# multiprocessing not safe
# trying to make it safe
while True:
    try:
        dir = fetch_fsaverage()
        break
    except (json.decoder.JSONDecodeError, RuntimeError):
        time.sleep(0.1)

subjects_dir = os.path.join(dir, '..')
transform_path = os.path.join(dir, 'bem', 'fsaverage-trans.fif')
transform = mne.read_trans(transform_path, return_all=False, )
lut_inv_dict = read_freesurfer_lut()[0]
label_lut = {v: k for k, v in lut_inv_dict.items()}
parcelation_fsaverage_img = nibabel.load(os.path.join(subjects_dir, 'fsaverage', 'mri', 'aparc.a2005s+aseg.mgz'))


def get_label_from_head_position_atlasreader(positions, atlas='aal'):
    """ position - array [N, 3] of N 3 X, Y Z positions in headspace, works for things in FSAVERAGE head space"""
    mni_dip_pose = mne.head_to_mni(positions, 'fsaverage', transform, subjects_dir=subjects_dir, verbose=None)
    labels = read_atlas_peak(atlas, mni_dip_pose)
    if isinstance(labels, str):
        labels = [labels]
    return labels


def get_label_from_head_position_freesurfer_parcelations(positions):
    """ position - array [N, 3] of N 3 X, Y Z positions in headspace, works for things in FSAVERAGE head space"""
    mni_dip_pose = mne.head_to_mni(positions, 'fsaverage', transform, subjects_dir=subjects_dir, verbose=None)
    mri_vox_t = np.linalg.inv(parcelation_fsaverage_img.header.get_vox2ras_tkr())
    vox_dip_pos = mne.transforms.apply_trans(mri_vox_t, mni_dip_pose).astype(int)
    vol_values = parcelation_fsaverage_img.get_fdata()[tuple(vox_dip_pos.T)]
    labels = [label_lut.get(i, 'Unknown') for i in vol_values]
    return labels


# labels = mne.read_labels_from_annot('fsaverage', subjects_dir=subjects_dir)
#
# img = nibabel.load(os.path.join(subjects_dir, 'fsaverage', 'mri', 'T1.mgz'))
# # img = nibabel.load(os.path.join(subjects_dir, 'fsaverage', 'mri', 'aparc.a2005s+aseg.mgz'))
# mri_vox_t = np.linalg.inv(img.header.get_vox2ras_tkr())
# vox_dip_pos = mne.transforms.apply_trans(mri_vox_t, mni_dip_pose).astype(int)
# vol_values = img.get_fdata()[tuple(vox_dip_pos.T)]
# lut = read_freesurfer_lut() 2 dicts, for color and int value for parcelisation try again and make pull request for mne
# lut[vol_values[0]]
# read_atlas_peak('harvard_oxford', mni_dip_pose)
#
# dip = mne.Dipole([0], dipole_pos, [1,], [dipole_ori], [100], name=None, conf=None, khi2=None, nfree=None, verbose=None)
# dip.plot_locations(transform, 'fsaverage', subjects_dir, )
#
# import  pylab as pb
# ax = pb.gca()
# ax.set_title(str(read_atlas_peak('aal', mni_dip_pose)))
# pb.show()

# https://github.com/mne-tools/mne-python/blob/3ea17053b81df811e4a13f01985bce2f2d0291e6/tutorials/source-modeling/plot_dipole_fit.py add to their tutorial?
