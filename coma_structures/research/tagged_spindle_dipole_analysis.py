import math

from mpl_toolkits.mplot3d import Axes3D
import argparse
import glob
import os
from collections import defaultdict
from platform import machine
from sklearn.linear_model import RANSACRegressor, LinearRegression
import mne
import numpy as np
import pickle
from mne.datasets import fetch_fsaverage
import pylab as pb
from pandas import DataFrame
# from scipy.stats import pearsonr as correlation_metric
from scipy.stats import spearmanr as correlation_metric
from tqdm import tqdm

from coma_structures.obci_readmanager.signal_processing.tags.read_tags_source import FileTagsSource
from datetimerange import DateTimeRange
from coma_structures.utils.data.brain import get_brain_pictures
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()


dir = fetch_fsaverage()
subjects_dir = os.path.join(dir, '..')
transform_path = os.path.join(dir, 'bem', 'fsaverage-trans.fif')
transform = mne.read_trans(transform_path, return_all=False, )

BRAIN_LENGTH = 200
BRAIN_WIDTH = 180
BRAIN_HEIGHT = 180

def savefig(file, fig, suffix, tagname):
    dirname = os.path.dirname(file)
    img_dir = os.path.join(dirname, 'img_tag_{}'.format(tagname))
    basename = os.path.basename(file)
    os.makedirs(img_dir, exist_ok=True)
    img_name = os.path.join(img_dir, "{}_{}.png".format(basename, suffix))
    fig.savefig(img_name)


def plot_scatter_correlation(dimension, frequency, amplitude, names, dim_label):
    unique_names = np.unique(names)
    fig = pb.figure(figsize=(12, 7))
    corr, pval = correlation_metric(dimension, frequency)
    additional_text = '\n'
    for name in unique_names:
        mask = name==names
        if np.sum(mask) < 3:
            continue
        try:
            lack_of_tagname = name == ''
            try:
                tagname_in_sleep_stadiums = name.split(';')[1] in ['1', '2', '3', '4', 'r', 'all'] and len(name.split(';'))==2
                stage = name.split(';')[1]
            except IndexError:
                tagname_in_sleep_stadiums = False
                stage = ''
            if tagname_in_sleep_stadiums or lack_of_tagname:
                pb.scatter(dimension[mask], frequency[mask], s=amplitude, alpha=0.3, label=stage)
                corr_stage, pval_stage = correlation_metric(dimension[mask], frequency[mask])
                additional_text = additional_text + "corr stage {}: {:.3f}, p_val: {:.4e}\n".format(stage, corr_stage, pval_stage)
        except IndexError:
            pass
    pb.legend()
    pb.title("{}[mm] vs f[Hz]\n corr: {:.3f}, p_val: {:.4e}".format(dim_label, corr, pval) + additional_text)
    pb.tight_layout()
    return fig


def filter_macroatoms_list(macroatoms, valid_time_windows):
    filtered = []
    for macroatom in tqdm(macroatoms, desc='filtering atoms in time windows (tags)'):
        position = macroatom.absolute_position.values[0]
        for time_window in valid_time_windows:
            if position >= time_window[0] and position <= time_window[1]:
                filtered.append(macroatom)
    return filtered


def add_tag_names_to_macroatoms(macroatoms, tags):
    if tags:
        for macroatom in tqdm(macroatoms, desc='assigning tags to atoms'):
            position = macroatom.absolute_position.values[0]
            macroatom_start = position - macroatom.width.values[0] / 2
            macroatom_end = position - macroatom.width.values[0] / 2
            macroatom_dtr = DateTimeRange(macroatom_start, macroatom_end)
            macroatom["tag_name"] = ''
            for tag in tags.get_tags(p_from=macroatom_start - 25, p_len=macroatom_end-macroatom_start + 25 * 2 ):
                tag_start = float(tag['start_timestamp'])
                if tag_start == 0:
                    tag_start += 0.001

                tag_dtd = DateTimeRange(tag_start, float(tag['end_timestamp']))
                intersection = tag_dtd.is_intersection(macroatom_dtr)
                if intersection:
                    macroatom["tag_name"] = macroatom["tag_name"] + ";" + tag['name']
    else:
        for macroatom in tqdm(macroatoms, desc='assigning tags to atoms'):
            macroatom["tag_name"] = ';all'


def scatter_3d(x, y, z, intercept, positions, frequencies, amplitude, file, tagname, draw_3d=True):
    brain_back, brain_side, brain_top = get_brain_pictures()
    unit_vector = np.array([x, y, z]) / np.linalg.norm(np.array([x, y, z]))
    projected = np.dot(positions.T, unit_vector)
    fig = pb.figure(figsize=(20, 15))
    if draw_3d:
        ax = fig.add_subplot(224, projection='3d')
        sc = ax.scatter(positions[0], positions[1], positions[2],  s=amplitude, alpha=0.2, c=frequencies)
        # sc = ax.scatter(unit_vector[0]*projected, unit_vector[1]*projected, unit_vector[2]*projected,  s=amplitude, alpha=0.2, c=frequencies)
        pb.colorbar(sc)
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        ax.quiver([0, ],
                  [0, ],
                  [0, ],
                  [unit_vector[0], ],
                  [unit_vector[1], ],
                  [unit_vector[2], ],
                  length=30.0,
                  normalize=True
                  )

        ax.quiver([0, ],
                  [0, ],
                  [0, ],
                  [x, ],
                  [y, ],
                  [z, ],
                  length=30.0,
                  normalize=True
                  )
        corr, pval = correlation_metric(projected, frequencies)
        pb.title("corr: {:.3f}, p_val: {:.4e}".format(corr, pval))

    ax = fig.add_subplot(222)
    sc = ax.scatter(positions[1], positions[2],  s=amplitude, alpha=0.2, c=frequencies)
    ax.set_xlabel('y')
    ax.set_ylabel('z')
    ax.arrow(0, 0, unit_vector[1] * 30, unit_vector[2] * 30, head_width=2, head_length=2)
    corr, pval = correlation_metric(projected, frequencies)
    pb.title("z boku")
    pb.xlim([-BRAIN_LENGTH / 2, BRAIN_LENGTH / 2])
    pb.ylim([-BRAIN_HEIGHT / 2, BRAIN_HEIGHT / 2])
    ax.imshow(brain_side, extent=[-180 / 2 - 19, 180 / 2 - 19, -120 / 2 + 11, 120 / 2 + 11], alpha=0.5)

    ax = fig.add_subplot(221)
    sc = ax.scatter(positions[0], positions[2],  s=amplitude, alpha=0.2, c=frequencies)
    ax.set_xlabel('x')
    ax.set_ylabel('z')
    ax.arrow(0, 0, unit_vector[0] * 30, unit_vector[2] * 30, head_width=2, head_length=2)
    pb.title('z tyłu')
    pb.xlim([-BRAIN_WIDTH / 2, BRAIN_WIDTH / 2])
    pb.ylim([-BRAIN_HEIGHT / 2, BRAIN_HEIGHT / 2])

    ax.imshow(brain_back, extent=[-140 / 2 + 1, 140 / 2 + 1, -100 / 2 + 18, 100 / 2 + 18], alpha=0.5)

    ax = fig.add_subplot(223)
    sc = ax.scatter(positions[0], positions[1],  s=amplitude, alpha=0.2, c=frequencies)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.arrow(0, 0, unit_vector[0] * 30, unit_vector[1] * 30, head_width=2, head_length=2)
    pb.title('z góry')
    ax.imshow(brain_top, extent=[-180 / 2 + 1, 180 / 2 + 1, -180 / 2 + 11 - 20, 180 / 2 + 11 - 20], alpha=0.5)

    pb.xlim([-BRAIN_WIDTH / 2, BRAIN_WIDTH / 2])
    pb.ylim([-BRAIN_LENGTH / 2, BRAIN_LENGTH / 2])

    pb.tight_layout()

    savefig(file, fig, '3d', tagname)
    pb.close(fig)


def get_major_direction(positions, frequencies):
    try:
        ransac = RANSACRegressor()
        ransac.fit(positions.T, frequencies)

        x, y, z = ransac.estimator_.coef_
        coef_summary = [x, y, z]
        intercept = ransac.estimator_.intercept_
    except:
        x, y, z = [0, 0, -1]
        coef_summary = [np.nan, np.nan, np.nan]
        intercept = -100

    try:
        linear_regressor = LinearRegression()
        linear_regressor.fit(positions.T, frequencies)

        x, y, z = linear_regressor.coef_
        coef_summary = [x, y, z]
    except:
        x, y, z = [0, 0, -1]
        coef_summary = [np.nan, np.nan, np.nan]
        intercept = -100

    unit_vector = np.array([x, y, z]) / np.linalg.norm(np.array([x, y, z]))
    return unit_vector, x, y, z, intercept, coef_summary


def build_hipnogram(tags, ax, tagnames):
    if ax is None:
        fig = pb.figure()
        ax = fig.add_subplot(111)
    time = []
    level = []
    levels = {'1': 4, '2': 3, '3': 2, '4': 1, 'r': 4.5, 'w': 5, 'm': 5.5}
    for tag in tags.get_tags():
        level_now = levels.get(tag['name'])
        if level_now is not None:
            time.append(np.mean([tag['start_timestamp'], tag['end_timestamp']]))
            level.append(levels.get(tag['name']))
    import datetime
    ax.plot([datetime.datetime.fromtimestamp(i) for i in time], level)
    ax.scatter([datetime.datetime.fromtimestamp(i) for i in time], level, s=4, color='k')
    import matplotlib.dates as md
    xfmt = md.DateFormatter('%H:%M:%S')
    ax.xaxis.set_major_formatter(xfmt)
    pb.sca(ax)
    pb.yticks([1,2,3,4,4.5,5, 5.5], ['S4', 'S3', 'S2', "S1", "REM", "WAKE", "MUSCLE"])
    pb.xlim([datetime.datetime.fromtimestamp(time[0]), datetime.datetime.fromtimestamp(time[-1])])
    for key in levels.values():
        pb.axhline(key, linestyle='--', alpha=0.3, color='gray')
    pb.title("Used tagnmames: {}".format(', '.join(tagnames)))


def calculate_spindle_params_per_time_windows(time_windows, mni_positions, frequencies, spindle_time, widths):
    angles = []
    correlation_coeff = []
    correlation_p = []
    n_spindles = []
    spindle_freq_median = []
    spindle_width_median = []
    for time_window in time_windows:
        spindle_mask = (spindle_time >= time_window[0]) * (spindle_time < time_window[1])
        masked_positions = mni_positions.T[spindle_mask].T
        masked_frequencies = frequencies[spindle_mask]

        direction_unit_vector, *_ = get_major_direction(masked_positions, masked_frequencies)

        direction_z = direction_unit_vector[2]
        direction_y = direction_unit_vector[1]

        # we want to transform to measure angle toward "down"
        # 90 degree would be forward, 270 - back
        # and we only want positive angle
        angle = np.arctan2(direction_y, -direction_z) / np.pi * 180 % 360

        angles.append(angle)
        projected = np.dot(masked_positions.T, direction_unit_vector)
        corr, pval = correlation_metric(projected, masked_frequencies)
        n = np.sum(spindle_mask)
        median = np.median(masked_frequencies)

        correlation_coeff.append(corr)
        correlation_p.append(pval)
        n_spindles.append(n)
        spindle_freq_median.append(median)

        masked_widths = widths[spindle_mask]
        spindle_width_median.append(np.median(masked_widths))

    return angles, correlation_coeff, correlation_p, n_spindles, spindle_freq_median, spindle_width_median


def draw_time_window_results(time_windows, results, results_names, fig, ax_first, p_threshold=0.05):
    time = []
    for time_window in time_windows:
        time.append(np.mean(time_window))
    import datetime
    time = [datetime.datetime.fromtimestamp(i) for i in time]

    for nr, (result, result_name) in enumerate(zip(results, results_names)):
        ax = fig.add_subplot(len(results) + 1, 1, nr + 2, sharex=ax_first)
        ax.plot(time, result)
        pb.title(result_name)
        if result_name == 'angles':
            ax.clear()
            idx_p_val = results_names.index('correlation_p')
            corr_p_mask = np.array(results[idx_p_val]) > p_threshold
            result_filtered = np.array(result)
            result_filtered[corr_p_mask] = np.nan
            ax.plot(time, result_filtered)
            ax.plot(time, result, alpha=0.2, zorder=0)
            pb.ylim([0, 360])
            for key in [90, 180, 270]:
                pb.axhline(key, linestyle='--', alpha=0.3, color='gray')
            pb.gca().invert_yaxis()

            amount_of_arrows = 20
            idx = np.round(np.linspace(0, len(result) - 1, amount_of_arrows)).astype(int)
            times_to_quiver = np.array(time)[idx]
            result_to_quiver = np.array(result_filtered)[idx]
            U = np.cos((result_to_quiver - 90) / 180 * np.pi)
            V = np.sin((result_to_quiver - 90) / 180 * np.pi)
            pb.quiver(times_to_quiver, result_to_quiver, U, V, scale=40, width=0.002)
            result_to_quiver = np.array(result)[idx]
            U = np.cos((result_to_quiver - 90) / 180 * np.pi)
            V = np.sin((result_to_quiver - 90) / 180 * np.pi)
            pb.quiver(times_to_quiver, result_to_quiver, U, V, scale=40, width=0.002, alpha=0.2)

        if result_name == 'correlation_coeff':
            pb.ylim([0, 1])
        if result_name == 'correlation_p':
            pb.ylim([0, 1])
        import matplotlib.dates as md
        xfmt = md.DateFormatter('%H:%M:%S')
        ax.xaxis.set_major_formatter(xfmt)
        pb.xlim([time[0], time[-1]])


def plot_sleep_profile_angle(spindle_time, mni_positions, tags, frequencies, widths, tag_filter='', windows_length_minutes=15,
                             windows_spacing_minutes=5, tagnames=''):

    time_windows = []
    max_time = spindle_time.max()
    for i in np.arange(0, max_time, windows_spacing_minutes * 60):
        time_windows.append([i, i + windows_length_minutes * 60])

    results = calculate_spindle_params_per_time_windows(time_windows, mni_positions, frequencies, spindle_time, widths)
    results_names = 'angles, correlation_coeff, correlation_p, n_spindles, spindle_freq_median, spindle_width_median'.split(', ')

    fig = pb.figure(figsize=(10,20))
    ax = fig.add_subplot(len(results) + 1, 1, 1)
    try:
        build_hipnogram(tags, ax, tagnames)
    except (IndexError, AttributeError):
        pass
    draw_time_window_results(time_windows, results, results_names, fig, ax)
    fig.tight_layout()
    return fig


def analyse_spindles(file, tags=None, tagname='2', analyzed_tagname='Spindle (wrzeciono)'):
    with open(file, 'rb') as pickle_read:
        macroatoms_list = pickle.load(pickle_read)
        spindles = [i[0] for i in macroatoms_list if i[1]==analyzed_tagname]

        # spindles = [i[0] for i in macroatoms_list if i[1]=='epileptic']
        # valid_time_windows = sorted([[2*60.0, 10000000000000000]])
        # spindles = filter_macroatoms_list(spindles, valid_time_windows)

        if tags:
            if tagname:
                tagnames = tagname.split(',')
                valid_time_windows = []
                for tagneme_split in tagnames:
                    valid_time_windows_tag = get_tag_timewindows(tags, tagneme_split)
                    valid_time_windows.extend(valid_time_windows_tag)
                valid_time_windows = sorted(valid_time_windows)
                spindles = filter_macroatoms_list(spindles, valid_time_windows)
                add_tag_names_to_macroatoms(spindles, None)
            else:
                add_tag_names_to_macroatoms(spindles, tags)
                tagnames = ''
        else:
            add_tag_names_to_macroatoms(spindles, None)
            tagnames=''

        dip_positions = np.array([i[['dip_posx', 'dip_posy', 'dip_posz']].values[0] for i in spindles])
        frequencies = np.array([i['frequency'].values[0] for i in spindles])
        widths = np.array([i['width'].values[0] for i in spindles])
        amplitudes = np.array([i['amplitude'].max() for i in spindles])
        names = np.array([i['tag_name'].max() for i in spindles])
        spindle_times = np.array([i['absolute_position'].values[0] for i in spindles])

        # x y z in MNI coordinate spac
        # the origin: anterior commissure
        # the x-axis increases from left to right
        # the y-axis increases from posterior to anterior (back to front)
        # the z-axis increases from inferior to superior (bottom to top)

        mni_dip_pose = mne.head_to_mni(dip_positions, 'fsaverage', transform,
                                       subjects_dir=subjects_dir, verbose=None)
        mni_x = mni_dip_pose[:, 0]
        mni_y = mni_dip_pose[:, 1]
        mni_z = mni_dip_pose[:, 2]

        positions = np.array([mni_x, mni_y, mni_z])

        file_name = os.path.basename(file)

        window_length = 30
        windows_spacing = 5
        fig = plot_sleep_profile_angle(spindle_times, positions, tags, frequencies, widths, tag_filter=tagname,
                                       windows_length_minutes=window_length,
                                       windows_spacing_minutes=windows_spacing,
                                       tagnames=tagnames
                                       )
        savefig(file, fig, "spindle_profile_{}_{}".format(window_length, windows_spacing), tagname)
        pb.close(fig)

        n = frequencies.shape[0]
        corr_d = dict()
        pval_d = dict()

        unit_vector, x, y, z, intercept, coef_summary = get_major_direction(positions, frequencies)
        projected = np.dot(positions.T, unit_vector)

        fig = pb.figure()
        pb.hist(projected, bins=30)
        pb.title("Position along main vector, spindles {}".format(tagname))
        savefig(file, fig, "histogram", tagname)
        pb.close(fig)


        fig = pb.figure()
        pb.hist(frequencies, bins=30)
        pb.title("Frequency along main vector, spindles {}".format(tagname))
        savefig(file, fig, "histogram_freq", tagname)
        pb.close(fig)

        scatter_3d(x, y, z, intercept, np.array([mni_x, mni_y, mni_z]), frequencies, amplitudes, file, tagname)

        for dim, name in zip([mni_x, mni_y, mni_z, projected], ['x', 'y', 'z', 'projected']): # k as in kx+b model
            corr, pval = correlation_metric(dim, frequencies)
            corr_d[name] = corr
            pval_d[name] = pval
            fig = plot_scatter_correlation(dim, frequencies, amplitudes, names, name)
            savefig(file, fig, name, tagname)
            pb.close(fig)

    return file_name, n, corr_d['x'], pval_d['x'], corr_d['y'], pval_d['y'], corr_d['z'], pval_d['z'], corr_d['projected'], pval_d['projected'], coef_summary, [mni_x, mni_y, mni_z]


def get_tag_timewindows(tags, name):
    tags_list = tags.get_tags()
    return [[float(i['start_timestamp']), float(i['end_timestamp'])] for i in tags_list if i['name'] == name]


def draw_vectors(df):
    vectors = df[['coeff_x', 'coeff_y', 'coeff_z']].values
    min = np.min(vectors)
    max = np.max(vectors)
    supermax = np.max([np.abs(max), np.abs(min)]) * 1.2
    norms = np.sqrt(np.sum(vectors**2, axis=0))
    norms = 1
    unit_vectors = vectors / norms

    vector_mean = np.mean(vectors, axis=0)

    arrow_size = supermax * 0.01
    fig = pb.figure(figsize=(20, 15))
    ax = fig.add_subplot(222)
    ax.set_xlabel('y')
    ax.set_ylabel('z')
    for unit_vector in unit_vectors:
        ax.arrow(0, 0, unit_vector[1], unit_vector[2], head_width=arrow_size, head_length=arrow_size, alpha=0.2)
    ax.arrow(0, 0, vector_mean[1], vector_mean[2], head_width=arrow_size, head_length=arrow_size)

    pb.ylim([-supermax, supermax])
    pb.xlim([-supermax, supermax])
    pb.title("z boku")

    ax = fig.add_subplot(221)
    ax.set_xlabel('x')
    ax.set_ylabel('z')
    for unit_vector in unit_vectors:
        ax.arrow(0, 0, unit_vector[0], unit_vector[2], head_width=arrow_size, head_length=arrow_size, alpha=0.2)
    ax.arrow(0, 0, vector_mean[0], vector_mean[2], head_width=arrow_size, head_length=arrow_size)
    pb.ylim([-supermax, supermax])
    pb.xlim([-supermax, supermax])
    pb.title('z tyłu')


    ax = fig.add_subplot(223)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    for unit_vector in unit_vectors:
        ax.arrow(0, 0, unit_vector[0], unit_vector[1], head_width=arrow_size, head_length=arrow_size, alpha=0.2)
    ax.arrow(0, 0, vector_mean[0], vector_mean[1], head_width=arrow_size, head_length=arrow_size)
    pb.ylim([-supermax, supermax])
    pb.xlim([-supermax, supermax])
    pb.title('z góry')
    pb.tight_layout()
    return fig

def main():
    # np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Spindle correlation analysis")
    parser.add_argument('files', nargs='+', metavar='file', help="pickle with list of [macroatom, label]")
    parser.add_argument('-t', '--tag-folder', action='store', type=str,
                        default='',
                        help='folders with tags')
    parser.add_argument('-f', '--filter-tag-name', action='store', type=str,
                        default='',
                        help='filter by tag name, empty for just coloring, valid tagnames w,r,1,2,3,4,m,'
                             'denoting sleep stages or muscle activity. Can provide comma seperated list of tagnames.')

    parser.add_argument('-n', '--analyzed-tag-name', action='store', type=str,
                        default='Spindle (wrzeciono)',
                        help='in case you want to analyse marked graphoelements in tags other than spindles')

    namespace = parser.parse_args()
    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files

    tagname = namespace.filter_tag_name
    summary_data = defaultdict(list)
    mni_coords = defaultdict(list)
    for file in tqdm(files_to_work):
        if namespace.tag_folder:
            name = os.path.splitext(os.path.basename(file))[0]
            tag_candidates = glob.glob(os.path.join(namespace.tag_folder, name + '*.tag'))
            assert len(tag_candidates) == 1
            tags = FileTagsSource(tag_candidates[0])
        else:
            tags = None
        result = analyse_spindles(file, tags, tagname, analyzed_tagname=namespace.analyzed_tag_name)
        summary_data['_name'].append(result[0])
        summary_data['_n'].append(result[1])
        summary_data['corr_x'].append(result[2])
        summary_data['pval_x'].append(result[3])
        summary_data['corr_y'].append(result[4])
        summary_data['pval_y'].append(result[5])
        summary_data['corr_z'].append(result[6])
        summary_data['pval_z'].append(result[7])
        summary_data['corr_projected'].append(result[8])
        summary_data['pval_projected'].append(result[9])
        summary_data['coeff_x'].append(result[10][0])
        summary_data['coeff_y'].append(result[10][1])
        summary_data['coeff_z'].append(result[10][2])
        mni_coords['mni_x'].extend(result[11][0])
        mni_coords['mni_y'].extend(result[11][1])
        mni_coords['mni_z'].extend(result[11][2])

    df = DataFrame.from_dict(summary_data)
    mni_df = DataFrame.from_dict(mni_coords)

    dirname = os.path.dirname(files_to_work[0])
    img_dir = os.path.join(dirname, 'img_tag_{}'.format(tagname))
    df.to_csv(os.path.join(img_dir, 'summary.csv'))
    mni_df.to_csv(os.path.join(img_dir, 'summary_mni_coords.csv'))
    fig, axs = pb.subplots(3, figsize=(10, 15))
    df[['_name', 'corr_x', 'corr_y', 'corr_z', 'corr_projected']].plot.bar(x='_name', y=['corr_x', 'corr_y', 'corr_z', 'corr_projected'], ax=axs[0])
    df[['_name', 'pval_x', 'pval_y', 'pval_z', 'pval_projected']].plot.bar(x='_name', y=['pval_x', 'pval_y', 'pval_z', 'pval_projected'], ax=axs[1])
    axs[1].axhline(0.05, linestyle='--', color='gray', zorder=10)
    df[['_name', 'pval_x', 'pval_y', 'pval_z', 'pval_projected']].plot.bar(x='_name', y=['pval_x', 'pval_y', 'pval_z', 'pval_projected'], ax=axs[2], logy=True)
    axs[2].axhline(0.05, linestyle='--', color='gray', zorder=10)

    axs[0].set_title("Tag: {}".format(tagname))
    pb.tight_layout()
    fig.savefig(os.path.join(img_dir, 'summary.png'))

    fig = pb.figure(figsize=(15, 8))
    ax = fig.add_subplot(121)
    df[['_name', 'corr_x', 'corr_y', 'corr_z', 'corr_projected']].boxplot(patch_artist=True,
                                                                          whiskerprops=dict(linewidth=2),
                                                                          boxprops=dict(linewidth=2),
                                                                          capprops=dict(linewidth=2),
                                                                          flierprops=dict(linewidth=2),
                                                                          medianprops=dict(linewidth=2),
                                                                          meanprops=dict(linewidth=2),
                                                                          )

    pb.ylim([-1, 1])
    ax = fig.add_subplot(122)
    df[['_name', 'pval_x', 'pval_y', 'pval_z', 'pval_projected']].boxplot(patch_artist=True,
                                                                          whiskerprops=dict(linewidth=2),
                                                                          boxprops=dict(linewidth=2),
                                                                          capprops=dict(linewidth=2),
                                                                          flierprops=dict(linewidth=2),
                                                                          medianprops=dict(linewidth=2),
                                                                          meanprops=dict(linewidth=2),
                                                                          )
    ax.axhline(0.05, linestyle='--', color='gray', zorder=10)
    pb.ylim([0, 1])
    pb.tight_layout()
    fig.savefig(os.path.join(img_dir, 'summary_boxplot.png'))
    pb.close(fig)

    fig = draw_vectors(df)
    fig.savefig(os.path.join(img_dir, 'summary_vectors.png'))
    pb.close(fig)



# todo find a base transformation which maximalizes correlation with frequency?
# kąt wektoru głównego kierunku + wartość korelacji w czasie, dla wszystkich spindli i tylko dla wybranych tagów
# potem może kwadratowe?
# później analiza klastrowa?



if __name__ == '__main__':
    main()


# python tagged_spindle_dipole_analysis.py -t /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/  /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/tags_experiments/tagged_sleep_spindles/*.pickle
'''
python tagged_spindle_dipole_analysis.py -f "2" -t /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/  /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/tags_experiments/tagged_sleep_spindles/*.pickle
python tagged_spindle_dipole_analysis.py -f "3" -t /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/  /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/tags_experiments/tagged_sleep_spindles/*.pickle
python tagged_spindle_dipole_analysis.py -f "4" -t /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/  /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/tags_experiments/tagged_sleep_spindles/*.pickle
python tagged_spindle_dipole_analysis.py -f "r" -t /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/  /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/tags_experiments/tagged_sleep_spindles/*.pickle
python tagged_spindle_dipole_analysis.py -f "1" -t /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/  /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/tags_experiments/tagged_sleep_spindles/*.pickle
python tagged_spindle_dipole_analysis.py -f "" -t /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/  /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/tags_experiments/tagged_sleep_spindles/*.pickle


python tagged_spindle_dipole_analysis.py -f "" -t /repo/coma/dane_pomiarowe_nocne_do_topografii_wrzecion/  /repo/coma/dane_pomiarowe_nocne_do_topografii_wrzecion/tags_experiments/tagged_sleep_spindles/*.pickle

python tagged_spindle_dipole_analysis.py -f "" -t /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/  /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/tags_experiments/tagged_sleep_spindles_no_amplitude_limit/*.pickle
python tagged_spindle_dipole_analysis.py -f "2" -t /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/  /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/tags_experiments/tagged_sleep_spindles_no_amplitude_limit/*.pickle
python tagged_spindle_dipole_analysis.py -f "3" -t /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/  /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/tags_experiments/tagged_sleep_spindles_no_amplitude_limit/*.pickle



python tagged_spindle_dipole_analysis.py -f "" /repo/coma/NOWY_OPUS_COMA/DANE_POLITECHNIKA/DANE_PACJENTOW_CLEAN/DANE/tags_experimental/PW-EEG-004/*.pickle
python tagged_spindle_dipole_analysis.py -f "" /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/tags_experiments/tags_spindles/mass_dataset/*.pickle

'''