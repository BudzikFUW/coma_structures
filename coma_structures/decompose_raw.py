import argparse
import glob
import json
import multiprocessing
import os
import tempfile
from subprocess import check_output

from shellescape import quote

from coma_structures.old.config import MP_EPOCH_IN_SECONDS, RESAMPLING_FREQUENCY
from coma_structures.utils.utils import read_raw, preanalyse_raw, get_mp_book_paths

this_folder = os.path.dirname(__file__)
USE_EMPI_V1 = True


def generate_mp_book(x, path_to_book, mp_type):
    tmp_dir = tempfile.mkdtemp()
    x.astype('float32').tofile(os.path.join(tmp_dir, 'signal.bin'))
    [sample_count, channel_count] = x.shape
    epoch_length = MP_EPOCH_IN_SECONDS * RESAMPLING_FREQUENCY
    if mp_type == 'smp':
        config = '''
energyError 0.05
maximalNumberOfIterations ''' + str(MP_EPOCH_IN_SECONDS * 5) + '''
energyPercent 99.9

MP SMP

nameOfDataFile signal.bin
nameOfOutputDirectory .
samplingFrequency ''' + str(RESAMPLING_FREQUENCY) + '''

numberOfChannels ''' + str(channel_count) + '''
selectedChannels 1-''' + str(channel_count) + ''' 

numberOfSamplesInEpoch ''' + str(epoch_length) + '''
selectedEpochs 1-''' + str(sample_count // epoch_length) + '''

minAtomScale 0.1
maxAtomScale 10.0
maxAtomFrequency 45.0
'''
    elif mp_type == 'mmp3':
        config = '''
energyError 0.05
maximalNumberOfIterations ''' + str(MP_EPOCH_IN_SECONDS * 5 * 5) + '''
energyPercent 99.9

MP MMP3

nameOfDataFile signal.bin
nameOfOutputDirectory .
samplingFrequency ''' + str(RESAMPLING_FREQUENCY) + '''

numberOfChannels ''' + str(channel_count) + '''
selectedChannels 1-''' + str(channel_count) + ''' 

numberOfSamplesInEpoch ''' + str(epoch_length) + '''
selectedEpochs 1-''' + str(sample_count // epoch_length) + '''

minAtomScale 0.1
maxAtomScale 10.0
maxAtomFrequency 45.0
'''
    elif mp_type == 'mmp1':
        config = '''
        energyError 0.05
        maximalNumberOfIterations ''' + str(MP_EPOCH_IN_SECONDS * 5 * 2) + '''
        energyPercent 99.9

        MP MMP1

        nameOfDataFile signal.bin
        nameOfOutputDirectory .
        samplingFrequency ''' + str(RESAMPLING_FREQUENCY) + '''

        numberOfChannels ''' + str(channel_count) + '''
        selectedChannels 1-''' + str(channel_count) + ''' 

        numberOfSamplesInEpoch ''' + str(epoch_length) + '''
        selectedEpochs 1-''' + str(sample_count // epoch_length) + '''

        minAtomScale 0.1
        maxAtomScale 10.0
        maxAtomFrequency 45.0
        '''

    if 'MMP' in config:
        suffix = 'mmp'
    else:
        suffix = 'smp'
    with open(os.path.join(tmp_dir, 'signal.cfg'), 'w') as config_file:
        config_file.write(config)
    this_folder = os.path.dirname(__file__)
    mp_path = os.path.join(this_folder, 'bin_lin64', 'empi-0.9.6-lin64', 'empi-lin64')
    invocation = 'cd ' + quote(
            tmp_dir) + ' && {} signal.cfg && mv signal_{}.db '.format(mp_path, suffix) + quote(
        path_to_book)
    print("running:")
    print(invocation)
    if os.system(invocation):
        raise Exception('empi invocation failed')
    return config


def generate_mp_book_empi_v1(x, path_to_book, mp_type):
    tmp_dir = tempfile.mkdtemp()
    x.astype('float32').tofile(os.path.join(tmp_dir, 'signal.bin'))
    [sample_count, channel_count] = x.shape
    epoch_length = MP_EPOCH_IN_SECONDS * RESAMPLING_FREQUENCY

    if 'mmp' in mp_type:
        suffix = 'mmp'
    else:
        suffix = 'smp'
    this_folder = os.path.dirname(__file__)
    maximum_iterations = MP_EPOCH_IN_SECONDS * 5 * 2
    sampling_frequency = RESAMPLING_FREQUENCY
    cpu_threads = multiprocessing.cpu_count()
    if mp_type == 'mmp1':
        mode = ' --mmp1'
    if mp_type == 'mmp3':
        mode = ' --mmp3'
    if mp_type == 'smp':
        mode = ''
    segment_size = int(epoch_length)
    empi_params = "-c {} -f {} -i {} -r 0.01{} --segment-size {} --gabor --cpu-threads 1 --cpu-workers {} --energy-error=0.05 --gabor-scale-max 10 --gabor-freq-max 45.0 --gabor-scale-min 0.1 -o global".format(channel_count, sampling_frequency, maximum_iterations, mode, segment_size, cpu_threads)
    mp_path = os.path.join(this_folder, 'bin_lin64', 'empi-1.0.x-lin64', 'empi-lin64')
    invocation = 'cd ' + quote(
            tmp_dir) + ' && {} {} signal.bin signal_{}.db&& mv signal_{}.db '.format(mp_path, empi_params, suffix, suffix) + quote(
        path_to_book)
    print("running:")
    print(invocation)
    if os.system(invocation):
        raise Exception('empi invocation failed')
    return empi_params


def decompose(files, outdir, montage, mp_type, unique_file_names=False):
    for path_to_raw in files:
        try:
            path_to_book, path_to_book_params = get_mp_book_paths(outdir, path_to_raw, montage, mp_type, unique_file_names=unique_file_names, force_db=True)
            if not os.path.exists(path_to_book):
                print("Decomposing ", path_to_raw)
                raw = read_raw(path_to_raw)
                raw = preanalyse_raw(raw, montage)

                x = raw.get_data().T * 1e6  # to microvolts

                if USE_EMPI_V1:
                    mp_config = generate_mp_book_empi_v1(x, path_to_book, mp_type)
                else:
                    mp_config = generate_mp_book(x, path_to_book, mp_type)
                if not os.path.exists(path_to_book_params):
                    with open(path_to_book_params, 'w') as config_file:
                        commit = check_output('git rev-parse HEAD', cwd=os.path.dirname(os.path.abspath(__file__)), shell=True).decode().strip()
                        json.dump({'mp_config': mp_config, 'channel_names': raw.ch_names, 'run_commit': commit}, config_file, indent=2)
        except:
            print("Analysed file:", path_to_raw)
            raise


def main():
    parser = argparse.ArgumentParser(description="Decomposing control files.")
    parser.add_argument('files', nargs='+', metavar='file', help="path to *.raw .bin files")
    parser.add_argument('-o', '--outdir', nargs='?', type=str, help='MP Output directory', )
    parser.add_argument('-m', '--montage', action='store', help='Montages: none, ears, transverse', default='none')
    parser.add_argument('-mp', '--mp-type', action='store', help='mp type: smp, mmp3', default='smp')
    parser.add_argument('-u', '--unique-file-names', action='store', type=str,
                        default='y',
                        help='are raw filenames unique? If no MP folder will contain subfolders')
    namespace = parser.parse_args()
    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files
    decompose(files_to_work, namespace.outdir, namespace.montage, namespace.mp_type, namespace.unique_file_names == 'y')


if __name__ == '__main__':
    main()

# nohup python multi_ssh_processing.py "decompose_raw -m transverse -mp mmp3 -o /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_stare_do_testu/converted/mp" /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_stare_do_testu/converted/*.raw &
# nohup python multi_ssh_processing.py "decompose_raw -u n -m none -mp mmp1 -o /dmj/fizmed/mdovgialo/projekt_doktorat/dane_do_odtworzenia_eeg_z_eeg/mp" /dmj/fizmed/mdovgialo/projekt_doktorat/dane_do_odtworzenia_eeg_z_eeg/*/*/*/*.obci.raw &
# nohup python multi_ssh_processing.py "decompose_raw -u y -m none -mp mmp1 -o /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/mp_v1.0.x" /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_kontrola_stare/converted/*.bin&
# nohup nice python multi_ssh_processing.py "decompose_raw -u y -m none -mp mmp1 -o /repo/coma/dane_pomiarowe_nocne_do_topografii_wrzecion/mp_v1.0.x" /repo/coma/dane_pomiarowe_nocne_do_topografii_wrzecion/*.raw&


# nohup nice python multi_ssh_processing.py "decompose_raw -u n -m none -mp mmp1 -o /repo/coma/NOWY_OPUS_COMA/DANE_POLITECHNIKA/DANE_PACJENTOW_CLEAN/DANE/MP" /repo/coma/NOWY_OPUS_COMA/DANE_POLITECHNIKA/DANE_PACJENTOW_CLEAN/DANE/PW-EEG-04*/EEG/*.raw&
# nohup nice python multi_ssh_processing.py "decompose_raw -u n -m none -mp mmp1 -o /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/mp" /repo/coma/NOWY_OPUS_COMA/MASS_DATASET/mass_dataset/converted/*.raw&
# nohup nice python multi_ssh_processing.py "decompose_raw -u n -m none -mp mmp1 -o /dmj/fizmed/mdovgialo/projekt_usuwanie_alfy_mp_plakat/dane_test/dane_N400_dla_M/MP" /dmj/fizmed/mdovgialo/projekt_usuwanie_alfy_mp_plakat/dane_test/dane_N400_dla_M/p*/*.raw&

# nohup nice python multi_ssh_processing.py "decompose_raw -u n -m none -mp mmp1 -o /repo/coma/NOWY_OPUS_COMA/DANE_NOWOWIEJSKA/SOMNAMBULIZM/converted/MP" /repo/coma/NOWY_OPUS_COMA/DANE_NOWOWIEJSKA/SOMNAMBULIZM/converted/*.raw &
# nohup nice python multi_ssh_processing.py "decompose_raw -u n -m none -mp mmp1 -o /repo/coma/NOWY_OPUS_COMA/DANE_NOWOWIEJSKA/KONTROLA/converted/MP" /repo/coma/NOWY_OPUS_COMA/DANE_NOWOWIEJSKA/KONTROLA/converted/*.raw &

# nohup nice python multi_ssh_processing.py "decompose_raw -u n -m none -mp mmp1 -o /dmj/fizmed/mdovgialo/projekt_usuwanie_alfy_mp_plakat/dane_test/n400-slowa/MP" /dmj/fizmed/mdovgialo/projekt_usuwanie_alfy_mp_plakat/dane_test/n400-slowa/p*/*.raw&


# decompose_raw -m none -mp mmp1 -o /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/mp_0.9.5/ /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/e1.raw
# time nice decompose_raw -m none -mp mmp1 -o /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/mp_0.9.6/ /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/e1.raw

# decompose_raw -u n -m none -mp mmp1 -o /dmj/fizmed/mdovgialo/projekt_usuwanie_alfy_mp_plakat/dane_test/p300-sluchowe-tony/p300-sluchowe-slowa/MP /dmj/fizmed/mdovgialo/projekt_usuwanie_alfy_mp_plakat/dane_test/p300-sluchowe-tony/p300-sluchowe-slowa/PD/PD_P300_sl_2016_Jul_18_1620.obci.raw

# 0.9.2 - e1.raw (35 minut):
# real	22m4.785s
# user	47m51.530s
# sys	1m31.399s

# 0.9.6 - e1.raw
# real	46m0.531s
# user	122m29.819s
# sys	1m25.964s


