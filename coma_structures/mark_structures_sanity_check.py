import glob

import numpy as np
import argparse

import pandas

from coma_structures.mark_structures_nn import \
    read_cache_or_create_parametrised_and_save, make_tags_from_atoms
from coma_structures.obci_readmanager.signal_processing.tags import tags_file_writer as tags_writer
from coma_structures.utils.utils import TAG_DEFS, classes_d


def main():
    np.set_printoptions(precision=3)
    np.set_printoptions(suppress=True)
    np.set_printoptions(linewidth=120)
    parser = argparse.ArgumentParser(description="Mark_using nn")
    parser.add_argument('-d', '--mp-dir', nargs='?', type=str, help='MP Output directory')
    parser.add_argument('-m', '--montage', action='store', help='Montages: none, ears, transverse', default='none')
    parser.add_argument('-mp', '--mp-type', action='store', help='mp type: smp, mmp3', default='smp')
    parser.add_argument('-o', '--output',  action='store', help='tags_output', default='')
    parser.add_argument('-c', '--cache', action='store', help='features cache folder to accelerate testing', default='')

    parser.add_argument('files', nargs='+', metavar='file', help="path to *.raw")

    namespace = parser.parse_args()
    if len(namespace.files) == 1:
        files_to_work = glob.glob(namespace.files[0])
    else:
        files_to_work = namespace.files
    print(files_to_work)
    for file in files_to_work:

        examples_nn, macroatoms, raw, preanalysed_raw = read_cache_or_create_parametrised_and_save(file, namespace)
        basename = os.path.basename(file)
        # if true record both styles of tags
        tag_types = list(sorted(classes_d.keys()))
        tags_all = []
        macroatoms_for_tags_all_tags = []
        for tag_type_nr, tag_type in enumerate(tag_types):
            if tag_type == 'None':
                continue
            predictions = predictions_second_layer = np.zeros((examples_nn.shape[0], len(tag_types)))
            predictions[:, tag_type_nr] = 1.0

            tags, macroatoms_for_tags = make_tags_from_atoms(predictions, predictions_second_layer,
                                        macroatoms, examples_nn,
                                        preanalysed_raw,
                                        raw,
                                        sureness_only=False,
                                        sureness_threshold=0.4,
                                        mark_none=False)
            macroatoms_for_tags_all_tags.append(macroatoms_for_tags)
            tags_all.extend(tags)

        def tag_sort_key(tag):
            return tag['start_timestamp']

        tags_all.sort(key=tag_sort_key)
        tags_all_filtered = []
        for tag in tags_all:
            if tag['start_timestamp'] > 0:
                tags_all_filtered.append(tag)
        os.makedirs(namespace.output, exist_ok=True)
        writer = tags_writer.TagsFileWriter(os.path.join(namespace.output,
                                                         os.path.splitext(
                                                             os.path.basename(
                                                                 file))[
                                                             0] + '.tag'),
                                            p_defs=TAG_DEFS)
        for tag in tags_all_filtered:
            writer.tag_received(tag)
        writer.finish_saving(0.0)

        print("Writing macroatoms per tags for {}".format(basename))
        macroatoms_output_dir_path = os.path.join(namespace.output, 'macroatoms')
        os.makedirs(macroatoms_output_dir_path, exist_ok=True)
        macroatoms_for_tags_all_tags = pandas.concat(macroatoms_for_tags_all_tags)
        macroatoms_for_tags_all_tags.to_pickle(os.path.join(macroatoms_output_dir_path, basename + '.bz2'))



if __name__ == '__main__':
    # turn off gpu for now
    import os
    os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
    main()


   # python mark_structures_sanity_check.py -d D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\mp -m none -mp mmp1 -o D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\tags_experiments\tags_test_temp_sanity_check -c D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\feature_cache_mmp1_dip  D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\*.raw
   # python mark_structures_sanity_check.py -d /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/mp -m none -mp mmp3 -o /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/tags_experiments/tags_sanity_check_only_higher -c D:\Marian\Documents\uniwerek\grant_grafoelementy_spiaczka\dane_docelowe\control_children\converted\feature_cache_dip_avr -c /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/feature_cache_dip_avr_centers/ /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/control_children/e1002348.raw
   # python mark_structures_sanity_check.py -d /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_stare_do_testu/converted/mp -m none -mp mmp1 -o /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_stare_do_testu/converted/tags_experiments/tags_sanity_check_only_higher -c /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_stare_do_testu/converted/feature_cache_dip_avr_centers/ /repo/coma/NOWY_OPUS_COMA/DANE_DOCELOWE/dane_snu_stare_do_testu/converted/*.raw