#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse

import matplotlib.pylab as py
import pandas as pd
import numpy as np
import json
import IPython
import os
import glob
import string


WAVE_TYPES = ['spindle', 'SWA']#, 'delta', 'theta', 'alpha', 'beta']
PROFILE_TYPES = ['counts', 'percent']
ylabels = {'spindle':'num. of spindles/3min', 'SWA':'%SWA/20sec'}#, 'SWA':'%SWA/20sec', 'Amp/3min', 'Amp/3min', 'Amp/3min', 'Amp/3min'}

pages_db = './pages_db.json'

# channels_db = "./channels_db.json"

fs = 128.0


def create_plots(name, directory, annotate=True):
    data_name = "{}/{}".format(directory, name) + "_{}_hipnogram.csv"

    if annotate:
        global_dir = "/".join(directory.split("/")[:-1])
        params_csv = os.path.join(global_dir, 'params', name + "_params.csv")
        params = pd.read_csv(params_csv)
        params.drop([u'Unnamed: 0', u'frequency_mse_spindle', u'spectral_entropy', u'theta_rel_power', \
                     u'alpha_rel_power', u'beta2_rel_power', u'beta1_rel_power', u'delta_rel_power'], axis=1, inplace=True)

    fig, axs = py.subplots(len(WAVE_TYPES), 1, figsize=(11.69, 8.27))
    # d = params.filter(regex=wave_type)
    text = []
    for c in params.columns:
        text.append(c + ' = %.4f' % (params[c].tolist()[0]))
    for (i, wave_type) in enumerate(WAVE_TYPES):
        hipnogram = pd.read_csv(data_name.format(wave_type))
        occ = hipnogram['occurences']
        names_occ = hipnogram['time']
        bar_width = names_occ[2]-names_occ[1]
        axs[i].bar(names_occ, occ, width=bar_width, color='#c8c6d1', edgecolor='#626166')
        axs[i].set_xlim([names_occ.iloc[0], names_occ.iloc[-1]])
        axs[i].set_ylabel(ylabels[wave_type])
        if i==(len(WAVE_TYPES)-1): axs[i].set_xlabel('time [hours]')
    axs[i].annotate('\n'.join(text), xy=(0.3, 1), xytext=(0.7, 0.8), xycoords='figure fraction', fontsize=12)

    return fig


def create_amplitude_plots(name, directory, annotate=True):
    data_name = "{}/{}".format(directory, name) + "_{}_occ.csv"

    with open(pages_db) as f:
        key = name.split('.')[0]
        pages_data = json.load(f)
        total_len = int(pages_data[key] * 20) #in sec

    if annotate:
        global_dir = "/".join(directory.split("/")[:-1])
        params_csv = os.path.join(global_dir, 'params', name + "_params.csv")
        params = pd.read_csv(params_csv)
        params.drop([u'Unnamed: 0', u'frequency_mse_spindle', u'spectral_entropy', u'theta_rel_power', \
                     u'alpha_rel_power', u'beta2_rel_power', u'beta1_rel_power', u'delta_rel_power'], axis=1, inplace=True)

    fig, axs = py.subplots(len(WAVE_TYPES), 1, figsize=(11.69, 8.27))
    # d = params.filter(regex=wave_type)
    text = []
    for c in params.columns:
        text.append(c + ' = %.4f' % (params[c].tolist()[0]))

    for (i, wave_type) in enumerate(WAVE_TYPES):
        t = np.linspace(0, total_len, total_len * 128.) #in samples
        x = np.zeros(len(t))
        occ = pd.read_csv(data_name.format(wave_type))
        for index, row in occ.iterrows():
            idx = np.abs(t -row["absolute_position"]).argmin()
            x[idx] += row["amplitude"]
        axs[i].plot(t, x)
        if i==(len(WAVE_TYPES)-1): axs[i].set_xlabel('time [sec]')
    axs[i].annotate('\n'.join(text), xy=(0.3, 1), xytext=(0.7, 0.8), xycoords='figure fraction', fontsize=12)

    return fig


def profile_energy_plot(name, directory, additional_parameter='energy', annotate=True):

    SMALL_SIZE = 10
    MEDIUM_SIZE = 11
    BIGGER_SIZE = 14

    py.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
    py.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
    py.rc('axes', labelsize=MEDIUM_SIZE)     # fontsize of the x and y labels
    py.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
    py.rc('ytick', labelsize=SMALL_SIZE)     # fontsize of the tick labels
    py.rc('legend', fontsize=SMALL_SIZE)     # legend fontsize
    py.rc('figure', titlesize=BIGGER_SIZE)   # fontsize of the figure title

    #data_occ = "{}/{}".format(directory, name) + "_{}_occ_sel.csv"
    data_occ = "{}/{}".format(directory, name) + "_{}_occ_sel.csv"

    with open(pages_db) as f:
        key = name.split('.')[0]
        pages_data = json.load(f)
        total_len = int(pages_data[key] * 20) #in sec

    fig, axs = py.subplots(2 * len(WAVE_TYPES), 1, figsize=(11.69, 8.27))

    s_i = 0
    s_i_text = 0

    frac = 0.9
    for (i, wave_type) in enumerate(WAVE_TYPES):
        data = pd.read_csv(data_occ.format(wave_type))
        if wave_type=='spindle':
            histogram = _get_histogram_values_in_sec(data, total_len, 20, 'counts')
        elif wave_type == 'SWA':
            histogram = _get_histogram_values_in_sec(data, total_len, 20, 'percent')
        else:
            histogram = _get_histogram_values_in_sec(data, total_len, 20, 'counts')
        counts = histogram['occurences']
        names_occ = histogram['time'] / 3600
        bar_width = names_occ[2] - names_occ[1]
        axs[s_i].bar(names_occ, counts, width=bar_width, color='#c8c6d1', edgecolor='#626166')
        axs[s_i].set_xlim([names_occ.iloc[0], names_occ.iloc[-1]])
        if wave_type == "spindle":
            axs[s_i].set_ylim([0, 15])
            axs[s_i].set_ylabel('num. of sleep\nspindles / 20 s')
        elif wave_type == 'SWA':
            axs[s_i].set_ylim([0, 100])
            axs[s_i].axhline(y=20., color='k', linestyle='-', linewidth=0.5)
            axs[s_i].axhline(y=50., color='k', linestyle='-', linewidth=0.5)
            axs[s_i].set_ylabel('%SWA / 20 s')
        else:
            axs[s_i].set_ylim([0, 15])
            axs[s_i].set_ylabel('num. of theta\nwaves / 20 s')
        s_i += 1

        data = pd.read_csv(data_occ.format(wave_type))
        if additional_parameter == 'energy':
            histogram_energy = _get_histogram_values_in_sec(data, total_len, 20, 'energy')
            t = histogram_energy['time'] / 3600
            bar_width = t[2] - t[1]
            axs[s_i].bar(t, histogram_energy['occurences'], width=bar_width, color='#b2b2b2', edgecolor='#b2b2b2')
            axs[s_i].set_xlim([t.iloc[0], t.iloc[-1]])
            axs[s_i].set_ylabel('power [$\mathsf{\mu V^2}$]')
        elif additional_parameter == 'amplitude':
            t = np.linspace(0, total_len, total_len)
            x = np.zeros(len(t))
            for index, row in data.iterrows():
                idx = np.abs(t -row["absolute_position"]).argmin()
                x[idx] += row["amplitude"]
            t = t / 3600
            axs[s_i].plot(t, x, color='#b2b2b2')
            axs[s_i].set_xlim([t[0], t[-1]])
            axs[s_i].set_ylabel('Amplitude [$\mathsf{\mu V}$]')
            if wave_type == "spindle":
                axs[s_i].set_ylim([0, 150])
            elif wave_type == 'SWA':
                axs[s_i].set_ylim([0, 800])
            else:
                axs[s_i].set_ylim([0, 100])

        s_i += 1
        if s_i==(2 * len(WAVE_TYPES)): axs[s_i-1].set_xlabel('time [hours]')

    for n in xrange(4):
        axs[n].text(-0.10, 0.45, string.ascii_lowercase[n], transform=axs[n].transAxes, size=14)

    if annotate:

        global_dir = "/".join(directory.split("/")[:-1])
        params_csv = os.path.join(global_dir, 'params', name + "_params_s12_swa40.csv")
        params = pd.read_csv(params_csv)
        # params.drop([u'Unnamed: 0', u'frequency_mse_spindle', u'spectral_entropy', u'theta_rel_power', \
                     # u'alpha_rel_power', u'beta2_rel_power', u'beta1_rel_power', u'delta_rel_power'], axis=1, inplace=True)
        params_ss = params[[u'power_spindle', u'profile_dfa_spindle', u'calinski_harabaz_spindle']]

        params_swa = params[[u'power_SWA', u'profile_dfa_SWA', u'calinski_harabaz_SWA']]

        params_ds = params[[u'min_deep_sleep_20', u'dfa_deep_sleep_20',
                            u'min_deep_sleep_50', u'dfa_deep_sleep_50']]

        text = []
        for c in params_ss.columns:
            text.append(c + ' = %.4f' % (float(params_ss[c])))
        axs[1].annotate('\n'.join(text), xy=(0.3, 1), xytext=(0.75, 0.65), xycoords='axes fraction', fontsize=10)
        text = []
        for c in params_swa.columns:
            text.append(c + ' = %.4f' % (params_swa[c].tolist()[0]))
        axs[3].annotate('\n'.join(text), xy=(0.3, 1), xytext=(0.75, 0.65), xycoords='axes fraction', fontsize=10)
        text = []
        for c in params_ds.columns:
            text.append(c + ' = %.4f' % (params_ds[c].tolist()[0]))
        axs[2].annotate('\n'.join(text), xy=(0.3, 1), xytext=(0.75, 0.60), xycoords='axes fraction', fontsize=10)
    
    # py.tight_layout()

    return fig


def _get_histogram_values_in_sec(data, total_len, bin_width, profile_type='amplitude'):
    ### profile_type = 'amplitude', 'percent', 'counts', 'energy'

    number_of_bins = int(total_len / bin_width)
    bins = np.zeros(number_of_bins)
    time = np.linspace(0, float(number_of_bins * bin_width) - bin_width, len(bins))

    if profile_type == 'percent':
        for bin_, page in enumerate(time):
            time_page = np.linspace(page, page + bin_width, bin_width * fs)
            x = np.zeros(len(time_page))
            df = data[(data['offset'] > page) & (data['offset'] < (page + bin_width))]
            if len(df):
                for (index, row) in df.iterrows():
                    start = np.argmin(np.abs(time_page - float(row["offset"])))
                    end = np.argmin(np.abs(time_page - float(row["offset"] + row["struct_len"])))
                    if end > time_page[-1]: end = time_page[-1]
                    x[int(start):int(end)] = 1
            bins[bin_] = (float(np.sum(x==1)) / len(time_page)) * 100
    else:
        for (index, row) in data.iterrows():
            bin_index = int(np.floor(row['absolute_position'] / bin_width))
            if bin_index == len(time): bin_index = bin_index - 1
            if profile_type == 'amplitude':
                bins[bin_index] += row['amplitude']
            elif profile_type == 'counts':
                bins[bin_index] += 1
            elif profile_type == 'energy':
                bins[bin_index] += (row['modulus'] ** 2) / 128. / bin_width

    return pd.DataFrame({'time': time, 'occurences': bins})


def create_histograms(name, directory, orig_bin_width):
    data_name = "{}/{}_{}_occ.csv"
    hipnogram_out_name = "{}/{}".format(directory, name) + "_{}_hipnogram.csv"

    with open(pages_db) as f:
        key = name.split('.')[0]
        pages_data = json.load(f)
        total_len = float(pages_data[key] * 20) # in sec

    for (i, wave_type) in enumerate(WAVE_TYPES):
        data = pd.read_csv(data_name.format(directory, name, wave_type))
        histogram = _get_histogram_values_in_sec(data, total_len, orig_bin_width, PROFILE_TYPES[i])
        histogram.to_csv(hipnogram_out_name.format(wave_type))


if __name__ == '__main__':

    # occ_dir = '/dmj/fizmed/mzieleniewska/sleep/transients_occ/*_occ_sel.csv'

    paths_to_csv = ['/dmj/fizmed/mzieleniewska/sleep/transients_occ/MJ_02_04_2016_occ_sel.csv',
                    '/dmj/fizmed/mzieleniewska/sleep/transients_occ/MW_21_04_2017_occ_sel.csv',
                    '/dmj/fizmed/mzieleniewska/sleep/transients_occ/WS_15_06_2016_occ_sel.csv',
                    '/dmj/fizmed/mzieleniewska/sleep/transients_occ/RN_02_06_2016_occ_sel.csv']

    for path_to_csv in paths_to_csv:

        directory = os.path.dirname(path_to_csv)
        file_name = os.path.basename(path_to_csv)
        name = "_".join(file_name.split("_")[:4])

        print name

        # fig, ss_count, ss_amp, swa_amp = create_combo_plot(name, directory)
        fig = profile_energy_plot(name, directory, additional_parameter='energy', annotate=False)
        # fig.suptitle(name, fontsize=20)

        # fig = create_plots(name, directory)
        if not os.path.exists(os.path.join(directory, 'profiles')):
            os.makedirs(os.path.join(directory, 'profiles'))
        py.savefig(os.path.join(directory, 'profiles', name+'_profil_energia.eps'), format='eps', dpi=1000,
                   bbox_inches="tight")
        py.savefig(os.path.join(directory, 'profiles', name + '_profil_energia.svg'), format='svg', dpi=1000,
                   bbox_inches="tight")
        py.close(fig)

