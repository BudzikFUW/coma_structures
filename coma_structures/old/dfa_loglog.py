# coding: utf-8

import glob
import pandas as pd
import os
import nolds
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats

from helper_functions import get_total_number_of_epochs
from create_histograms import _get_histogram_values_in_sec

from nolds.measures import logarithmic_n, poly_fit, plot_reg


def dfa2(data, nvals=None, overlap=True, order=1, fit_trend="poly",
        fit_exp="RANSAC", debug_plot=False, debug_data=False, plot_file=None):
    """
    Performs a detrended fluctuation analysis (DFA) on the given data

    Recommendations for parameter settings by Hardstone et al.:
        * nvals should be equally spaced on a logarithmic scale so that each window
            scale hase the same weight
        * min(nvals) < 4 does not make much sense as fitting a polynomial (even if
            it is only of order 1) to 3 or less data points is very prone.
        * max(nvals) > len(data) / 10 does not make much sense as we will then have
            less than 10 windows to calculate the average fluctuation
        * use overlap=True to obtain more windows and therefore better statistics
            (at an increased computational cost)

    Explanation of DFA:
        Detrended fluctuation analysis, much like the Hurst exponent, is used to
        find long-term statistical dependencies in time series.

        The idea behind DFA originates from the definition of self-affine
        processes. A process X is said to be self-affine if the standard deviation
        of the values within a window of length n changes with the window length
        factor L in a power law:

        std(X,L * n) = L^H * std(X, n)

        where std(X, k) is the standard deviation of the process X calculated over
        windows of size k. In this equation, H is called the Hurst parameter, which
        behaves indeed very similar to the Hurst exponent.

        Like the Hurst exponent, H can be obtained from a time series by
        calculating std(X,n) for different n and fitting a straight line to the
        plot of log(std(X,n)) versus log(n).

        To calculate a single std(X,n), the time series is split into windows of
        equal length n, so that the ith window of this size has the form

        W_(n,i) = [x_i, x_(i+1), x_(i+2), ... x_(i+n-1)]

        The value std(X,n) is then obtained by calculating std(W_(n,i)) for each i
        and averaging the obtained values over i.

        The aforementioned definition of self-affinity, however, assumes that the
        process is	non-stationary (i.e. that the standard deviation changes over
        time) and it is highly influenced by local and global trends of the time
        series.

        To overcome these problems, an estimate alpha of H is calculated by using a
        "walk" or "signal profile" instead of the raw time series. This walk is
        obtained by substracting the mean and then taking the cumulative sum of the
        original time series. The local trends are removed for each window
        separately by fitting a polynomial p_(n,i) to the window W_(n,i) and then
        calculating W'_(n,i) = W_(n,i) - p_(n,i) (element-wise substraction).

        We then calculate std(X,n) as before only using the "detrended" window
        W'_(n,i) instead of W_(n,i). Instead of H we obtain the parameter alpha
        from the line fitting.

        For alpha < 1 the underlying process is stationary and can be modelled as
        fractional Gaussian noise with H = alpha. This means for alpha = 0.5 we
        have no correlation or "memory", for 0.5 < alpha < 1 we have a memory with
        positive correlation and for alpha < 0.5 the correlation is negative.

        For alpha > 1 the underlying process is non-stationary and can be modeled
        as fractional Brownian motion with H = alpha - 1.

    References:
        .. [dfa_1] C.-K. Peng, S. V. Buldyrev, S. Havlin, M. Simons,
                             H. E. Stanley, and A. L. Goldberger, “Mosaic organization of
                             DNA nucleotides,” Physical Review E, vol. 49, no. 2, 1994.
        .. [dfa_2] R. Hardstone, S.-S. Poil, G. Schiavone, R. Jansen,
                             V. V. Nikulin, H. D. Mansvelder, and K. Linkenkaer-Hansen,
                             “Detrended fluctuation analysis: A scale-free view on neuronal
                             oscillations,” Frontiers in Physiology, vol. 30, 2012.

    Reference code:
        .. [dfa_a] Peter Jurica, "Introduction to MDFA in Python",
             url: http://bsp.brain.riken.jp/~juricap/mdfa/mdfaintro.html
        .. [dfa_b] JE Mietus, "dfa",
             url: https://www.physionet.org/physiotools/dfa/dfa-1.htm
        .. [dfa_c] "DFA" function in R package "fractal"

    Args:
        data (array of float):
            time series
    Kwargs:
        nvals (iterable of int):
            subseries sizes at which to calculate fluctuation
            (default: logarithmic_n(4, 0.1*len(data), 1.2))
        overlap (boolean):
            if True, the windows W_(n,i) will have a 50% overlap,
            otherwise non-overlapping windows will be used
        order (int):
            (polynomial) order of trend to remove
        fit_trend (str):
            the fitting method to use for fitting the trends, either 'poly'
            for normal least squares polynomial fitting or 'RANSAC' for
            RANSAC-fitting which is more robust to outliers but also tends to
            lead to unstable results
        fit_exp (str):
            the fitting method to use for the line fit, either 'poly' for normal
            least squares polynomial fitting or 'RANSAC' for RANSAC-fitting which
            is more robust to outliers
        debug_plot (boolean):
            if True, a simple plot of the final line-fitting step will be shown
        debug_data (boolean):
            if True, debugging data will be returned alongside the result
        plot_file (str):
            if debug_plot is True and plot_file is not None, the plot will be saved
            under the given file name instead of directly showing it through
            `plt.show()`
    Returns:
        float:
            the estimate alpha for the Hurst parameter (alpha < 1: stationary
            process similar to fractional Gaussian noise with H = alpha,
            alpha > 1: non-stationary process similar to fractional Brownian
            motion with H = alpha - 1)
        (1d-vector, 1d-vector, list):
            only present if debug_data is True: debug data of the form
            `(nvals, fluctuations, poly)` where `nvals` are the values used for
            log(n), `fluctuations` are the corresponding log(std(X,n)) and `poly`
            are the line coefficients (`[slope, intercept]`)
    """
    total_N = len(data)
    if nvals is None:
        nvals = logarithmic_n(4, 0.1 * total_N, 1.2)
    # create the signal profile
    # (cumulative sum of deviations from the mean => "walk")
    walk = np.cumsum(data - np.mean(data))
    fluctuations = []
    for n in nvals:
        # subdivide data into chunks of size n
        if overlap:
            # step size n/2 instead of n
            d = np.array([walk[i:i + n] for i in range(0, len(walk) - n, n // 2)])
        else:
            # non-overlapping windows => we can simply do a reshape
            d = walk[:total_N - (total_N % n)]
            d = d.reshape((total_N // n, n))
        # calculate local trends as polynomes
        x = np.arange(n)
        tpoly = [poly_fit(x, d[i], order, fit=fit_trend)
                         for i in range(len(d))]
        tpoly = np.array(tpoly)
        trend = np.array([np.polyval(tpoly[i], x) for i in range(len(d))])
        # calculate standard deviation ("fluctuation") of walks in d around trend
        flucs = np.sqrt(np.sum((d - trend) ** 2, axis=1) / n)
        # calculate mean fluctuation over all subsequences
        f_n = np.sum(flucs) / len(flucs)
        fluctuations.append(f_n)
    fluctuations = np.array(fluctuations)
    # filter zeros from fluctuations
    nonzero = np.where(fluctuations != 0)
    nvals = np.array(nvals)[nonzero]
    fluctuations = fluctuations[nonzero]
    if len(fluctuations) == 0:
        # all fluctuations are zero => we cannot fit a line
        poly = [np.nan, np.nan]
    else:
        poly = poly_fit(np.log(nvals), np.log(fluctuations), 1,
                        fit=fit_exp)
    if debug_plot:
        plot_reg(np.log(nvals), np.log(fluctuations), poly, "log(n)", "std(X,n)",
                fname=plot_file)
    if debug_data:
        return (poly[0], (np.log(nvals), np.log(fluctuations), poly))
    else:
        return poly[0]


if __name__ == '__main__':

    for file_name in glob.glob(os.path.expanduser('~/sleep/transients_occ/*SWA_occ_sel.csv')):
        df = pd.read_csv(file_name, index_col=0)
        p_name = '_'.join(os.path.basename(file_name).split('_')[:4])
        total_len = int(get_total_number_of_epochs(p_name) * 20)
        df_hist = _get_histogram_values_in_sec(df, total_len, 20, profile_type='percent')
        occ = df_hist.occurences
        time = df_hist.time

        alpha, dfa_data = dfa2(occ, debug_data=True)
        x, y, poly = dfa_data
        slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x, y)

        plt.subplot(211)
        plt.title("{} {} {}".format(alpha, r_value, p_value))

        plt.plot(x, y, "bo", label="data")
        plt.plot(x, slope*x + intercept, "r-", label="regression line")
        plt.xlabel("log(n)")
        plt.ylabel("std(X,n)")
        # plt.legend(loc="best")

        time = np.array(time) / 3600
        plt.subplot(212)
        plt.plot(time, occ)
        plt.xlim([0, time[-1]])
        plt.xlabel("time [h]")
        plt.ylabel("%SWA \n/20 sec")
        # plt.subplots_adjust(hspace=0.8)
        plt.show()
        plt.savefig(os.path.join(os.path.expanduser('~/sleep/transients_occ/dfa_swa_occ/'), p_name + '_SWA.svg'),
                    format='svg',
                    bbox_inches='tight')
        exit()
        plt.close()






