#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import matplotlib.gridspec as gridspec
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
import os


SMALL_SIZE = 13
MEDIUM_SIZE = 14
BIGGER_SIZE = 14

plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)     # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)     # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)     # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)   # fontsize of the figure title

OUTDIR = './figures/'


if __name__ == '__main__':

	df = pd.read_csv('./transients_occ/classification_parameters_s12_swa40.csv', index_col=0)

	#sns.set_style("whitegrid")

	df['regression_group'] = df.crs_group
	df.regression_group.loc[(df.crs_group=='MCS') | (df.crs_group=='EMCS')] = 'MCS/eMCS'
	df.regression_group.loc[(df.crs_group=='VS')] = 'UWS'

	sns.set(rc={'text.color':'black'})

	gs = gridspec.GridSpec(2, 6)
	x1, x2 = 0, 1

	linewidth = 1.0
	box_width = 0.4
	lw = 0.8
	col = 'black'
	hh = .04
	color="dimgray"

	ax1 = plt.subplot(gs[0, 0:2])
	sns.boxplot(x='regression_group', y='power_spindle', data=df, color=color, width=box_width, linewidth=linewidth)
	y, h = ax1.get_ylim()[0] + .93*(ax1.get_ylim()[1] - ax1.get_ylim()[0]), hh * (ax1.get_ylim()[1] - ax1.get_ylim()[0])
	plt.plot([x1, x1, x2, x2], [y, y+h, y+h, y], lw=lw, c=col)
	plt.text((x1+x2)*0.5, y+h, "***", ha='center', va='bottom', color=col)
	ax1.set_xlabel("")
	ax1.set_ylabel("spindle power\n[$\mathrm{\mu V^2}$]")

	ax2 = plt.subplot(gs[0, 2:4])

	sns.boxplot(x='regression_group', y='profile_dfa_spindle', data=df, color=color, width=box_width, linewidth=linewidth)
	ax2.set_ylim([0.48, 1.5])
	y, h = ax2.get_ylim()[0] + .93*(ax2.get_ylim()[1] - ax2.get_ylim()[0]), hh * (ax2.get_ylim()[1] - ax2.get_ylim()[0])
	plt.plot([x1, x1, x2, x2], [y, y+h, y+h, y], lw=lw, c=col)
	plt.text((x1+x2)*0.5, y+h, "*", ha='center', va='bottom', color=col)
	ax2.set_xlabel("")
	ax2.set_ylabel("spindles DFA")

	ax3 = plt.subplot(gs[0, 4:])

	sns.boxplot(x='regression_group', y='spectral_entropy', data=df, color=color, width=box_width, linewidth=linewidth)
	y, h = ax3.get_ylim()[0] + .93*(ax3.get_ylim()[1] - ax3.get_ylim()[0]), hh * (ax3.get_ylim()[1] - ax3.get_ylim()[0])
	plt.plot([x1, x1, x2, x2], [y, y+h, y+h, y], lw=lw, c=col)
	plt.text((x1+x2)*0.5, y+h, "***", ha='center', va='bottom', color=col)
	ax3.set_xlabel("")
	ax3.set_ylabel("spectral entropy")

	ax4 = plt.subplot(gs[1, 1:3])

	sns.boxplot(x='regression_group', y='profile_dfa_SWA', data=df, color=color, width=box_width, linewidth=linewidth)
	ax4.set_ylim([0.48, 1.5])
	y, h = ax4.get_ylim()[0] + .93*(ax4.get_ylim()[1] - ax4.get_ylim()[0]), hh * (ax4.get_ylim()[1] - ax4.get_ylim()[0])
	plt.plot([x1, x1, x2, x2], [y, y+h, y+h, y], lw=lw, c=col)
	plt.text((x1+x2)*0.5, y+h, "***", ha='center', va='bottom', color=col)
	ax4.set_xlabel("")
	ax4.set_ylabel("slow waves DFA")

	ax5 = plt.subplot(gs[1, 3:5])

	sns.boxplot(x='regression_group', y='min_deep_sleep_50', data=df, color=color, width=box_width, linewidth=linewidth)
	y, h = ax5.get_ylim()[0] + .93*(ax5.get_ylim()[1] - ax5.get_ylim()[0]), hh * (ax5.get_ylim()[1] - ax5.get_ylim()[0])
	plt.plot([x1, x1, x2, x2], [y, y+h, y+h, y], lw=lw, c=col)
	plt.text((x1+x2)*0.5, y+h, "***", ha='center', va='bottom', color=col)
	ax5.set_xlabel("")
	ax5.set_ylabel("time deep sleep\n[minutes]")

	fig = plt.gcf()
	gs.tight_layout(fig)
	fig.savefig(os.path.join(OUTDIR, "box_params.eps"), format='eps', dpi=1000, bbox_inches="tight")
	fig.savefig(os.path.join(OUTDIR, "box_params.svg"), format='svg', dpi=1000, bbox_inches="tight")
	plt.close(fig)


	#### plot classification output
	sns.set(rc={'axes.facecolor':'white', 'figure.facecolor':'white'})
	sns.set_style("whitegrid")

	df_class = pd.read_csv('./transients_occ/classification_output.csv', index_col=0)
	df_class["p"] = np.exp(df_class['classification_param']) / (1 + np.exp(df_class['classification_param']))

	df_class['regression_group'] = df_class.crs_group
	df_class.regression_group.loc[(df_class.crs_group=='MCS') | (df_class.crs_group=='EMCS')] = 'MCS/eMCS'
	df_class.regression_group.loc[(df_class.crs_group=='VS')] = 'UWS'

	# sns.set_context("poster", font_scale=1.5)
	sns.axes_style("whitegrid")
	a = ["#e6194b","#3cb44b","#ffe119","#0082c8","#f58231","#911eb4","#46f0f0","#f032e6","#d2f53c","#008080", 
		 "#342D7E","#aa6e28","#254117","#800000","#64E986","#808000","#000080","#808080","#000000", 
		 "#8A4117"] ##"EE9A4D"
	
	fig = plt.figure()#figsize=(40, 22))
	ax = fig.add_subplot(111)
	# ax.set_yscale("log")
	# plt.title(param_name)
	sns.swarmplot(x='regression_group', y='classification_param', data=df_class, hue='patient_code', palette=a, size=10)
	L = plt.legend(prop={'size': 10}, loc='center left', bbox_to_anchor=(0.88, 0.5))
	ax.set_ylabel("logit(p)")
	ax.set_xlabel("")
	plt.tight_layout()
	fig.savefig(os.path.join(OUTDIR, "classification_p.eps"), format='eps', dpi=1000, bbox_inches="tight")
	fig.savefig(os.path.join(OUTDIR, "classification_p.svg"), format='svg', dpi=1000, bbox_inches="tight")
	plt.close(fig)










