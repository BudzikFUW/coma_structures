#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import json

pages_db = "./pages_db.json"


def get_total_number_of_epochs(f_name):
	with open(pages_db) as f:
		pages_data = json.load(f)
		try:
			number_of_epochs = pages_data[f_name]
		except IOError:
			"No key in json file!"
	return number_of_epochs


def m_stationarity_l_simple(d):
    lin_scales = np.arange(10, 200)
    log_scales_args = np.arange(np.log10(200), np.log10(d.size / 2), 0.0023)
    log_scales_args = np.append(log_scales_args, log_scales_args[-1] + 0.0023)
    log_scales = (10. ** log_scales_args).astype(int)
    scales = np.concatenate((lin_scales, log_scales))
    
    max_non_stationarity_index = -np.inf
    for scale in scales:
        n = int(d.size / np.log(scale))
        starts = np.array(list(set(np.linspace(0, d.size - scale, n, dtype = int))))
        
        m_local = np.array([np.mean(d[start:start + scale]) for start in starts])
        s_local = np.median([np.std(d[start:start + scale], ddof = 1) for start in starts])
        
        s_m_local = m_local.std(ddof = 1)
        non_stationarity_index = s_m_local / (s_local / scale ** 0.5)
        
        if non_stationarity_index > max_non_stationarity_index:
            max_non_stationarity_index = non_stationarity_index
    return max_non_stationarity_index